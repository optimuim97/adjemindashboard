/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
var __webpack_exports__ = {};
/*!**********************************************!*\
  !*** ./resources/assets/js/orders/orders.js ***!
  \**********************************************/


var tableName = '#ordersTable';
$(tableName).DataTable({
  scrollX: true,
  deferRender: true,
  scroller: true,
  processing: true,
  serverSide: true,
  'order': [[0, 'asc']],
  ajax: {
    url: recordsURL
  },
  columnDefs: [{
    'targets': [43],
    'orderable': false,
    'className': 'text-center',
    'width': '8%'
  }],
  columns: [{
    data: 'customer_id',
    name: 'customer_id'
  }, {
    data: 'seller_id',
    name: 'seller_id'
  }, {
    data: 'seller_type',
    name: 'seller_type'
  }, {
    data: 'is_waiting',
    name: 'is_waiting'
  }, {
    data: 'current_status',
    name: 'current_status'
  }, {
    data: 'rating',
    name: 'rating'
  }, {
    data: 'notes',
    name: 'notes'
  }, {
    data: 'note_description',
    name: 'note_description'
  }, {
    data: 'note_date',
    name: 'note_date'
  }, {
    data: 'reports',
    name: 'reports'
  }, {
    data: 'report_description',
    name: 'report_description'
  }, {
    data: 'report_date',
    name: 'report_date'
  }, {
    data: 'payment_method_slug',
    name: 'payment_method_slug'
  }, {
    data: 'delivery_formula_slug',
    name: 'delivery_formula_slug'
  }, {
    data: 'delivery_fees',
    name: 'delivery_fees'
  }, {
    data: 'amount',
    name: 'amount'
  }, {
    data: 'currency_code',
    name: 'currency_code'
  }, {
    data: 'is_delivered',
    name: 'is_delivered'
  }, {
    data: 'is_waiting_payment',
    name: 'is_waiting_payment'
  }, {
    data: 'is_waiting_note',
    name: 'is_waiting_note'
  }, {
    data: 'delivery_date',
    name: 'delivery_date'
  }, {
    data: 'task_id',
    name: 'task_id'
  }, {
    data: 'merchant_assignments',
    name: 'merchant_assignments'
  }, {
    data: 'merchant_assignment_accepted',
    name: 'merchant_assignment_accepted'
  }, {
    data: 'reference',
    name: 'reference'
  }, {
    data: 'seller_has_delivred',
    name: 'seller_has_delivred'
  }, {
    data: 'seller_has_confirmed',
    name: 'seller_has_confirmed'
  }, {
    data: 'customer_has_confirmed_delivery',
    name: 'customer_has_confirmed_delivery'
  }, {
    data: 'customer_delivery_signature',
    name: 'customer_delivery_signature'
  }, {
    data: 'customer_delivery_signature_at',
    name: 'customer_delivery_signature_at'
  }, {
    data: 'is_customer_reported',
    name: 'is_customer_reported'
  }, {
    data: 'is_completed',
    name: 'is_completed'
  }, {
    data: 'is_seller_paid',
    name: 'is_seller_paid'
  }, {
    data: 'has_litige',
    name: 'has_litige'
  }, {
    data: 'is_refunded',
    name: 'is_refunded'
  }, {
    data: 'is_litige_solved',
    name: 'is_litige_solved'
  }, {
    data: 'litige_solution',
    name: 'litige_solution'
  }, {
    data: 'is_returned',
    name: 'is_returned'
  }, {
    data: 'is_customer_satisfied',
    name: 'is_customer_satisfied'
  }, {
    data: 'warranty_period_days',
    name: 'warranty_period_days'
  }, {
    data: 'warranty_period_last_date',
    name: 'warranty_period_last_date'
  }, {
    data: 'return_delivery_signature',
    name: 'return_delivery_signature'
  }, {
    data: 'return_delivery_signature_at',
    name: 'return_delivery_signature_at'
  }, {
    data: function data(row) {
      var url = recordsURL + row.id;
      var data = [{
        'id': row.id,
        'url': url + '/edit'
      }];
      return prepareTemplateRender('#ordersTemplate', data);
    },
    name: 'id'
  }]
});
$(document).on('click', '.delete-btn', function (event) {
  var recordId = $(event.currentTarget).data('id');
  deleteItem(recordsURL + recordId, tableName, 'Order');
});
/******/ })()
;