/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
var __webpack_exports__ = {};
/*!****************************************************!*\
  !*** ./resources/assets/js/boutiques/boutiques.js ***!
  \****************************************************/


var tableName = '#boutiquesTable';
$(tableName).DataTable({
  scrollX: true,
  deferRender: true,
  scroller: true,
  processing: true,
  serverSide: true,
  'order': [[0, 'asc']],
  ajax: {
    url: recordsURL
  },
  columnDefs: [{
    'targets': [6],
    'orderable': false,
    'className': 'text-center',
    'width': '8%'
  }],
  columns: [{
    data: 'nom',
    name: 'nom'
  }, {
    data: 'email',
    name: 'email'
  }, {
    data: 'identifiant',
    name: 'identifiant'
  }, {
    data: 'rccm',
    name: 'rccm'
  }, {
    data: 'customer_id',
    name: 'customer_id'
  }, {
    data: 'photo_profil',
    name: 'photo_profil'
  }, {
    data: function data(row) {
      var url = recordsURL + row.id;
      var data = [{
        'id': row.id,
        'url': url + '/edit'
      }];
      return prepareTemplateRender('#boutiquesTemplate', data);
    },
    name: 'id'
  }]
});
$(document).on('click', '.delete-btn', function (event) {
  var recordId = $(event.currentTarget).data('id');
  deleteItem(recordsURL + recordId, tableName, 'Boutique');
});
/******/ })()
;