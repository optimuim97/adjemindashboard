<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->word,
        'description' => $this->faker->text,
        'description_metadata' => $this->faker->text,
        'price' => $this->faker->word,
        'original_price' => $this->faker->word,
        'fees' => $this->faker->word,
        'currency_slug' => $this->faker->word,
        'old_price' => $this->faker->word,
        'has_promo' => $this->faker->word,
        'promo_percentage' => $this->faker->word,
        'medias' => $this->faker->text,
        'cover_id' => $this->faker->word,
        'cover_type' => $this->faker->word,
        'cover_url' => $this->faker->word,
        'cover_thumbnail' => $this->faker->word,
        'cover_duration' => $this->faker->word,
        'cover_width' => $this->faker->word,
        'cover_height' => $this->faker->word,
        'cover_blurhash' => $this->faker->word,
        'location_name' => $this->faker->word,
        'location_address' => $this->faker->word,
        'location_lat' => $this->faker->randomDigitNotNull,
        'location_lng' => $this->faker->randomDigitNotNull,
        'category_id' => $this->faker->word,
        'category_meta' => $this->faker->text,
        'customer_id' => $this->faker->word,
        'is_from_shop' => $this->faker->word,
        'is_sold' => $this->faker->word,
        'sold_at' => $this->faker->date('Y-m-d H:i:s'),
        'initiale_count' => $this->faker->word,
        'is_firm_price' => $this->faker->word,
        'condition_id' => $this->faker->randomDigitNotNull,
        'published_at' => $this->faker->date('Y-m-d H:i:s'),
        'is_published' => $this->faker->word,
        'deleted_by' => $this->faker->word,
        'deleted_creator' => $this->faker->word,
        'sold_count' => $this->faker->word,
        'delivery_services' => $this->faker->text,
        'slug' => $this->faker->word,
        'link' => $this->faker->word,
        'has_delivery' => $this->faker->word,
        'has_ordering' => $this->faker->word,
        'order_payment_methods' => $this->faker->text,
        'visibility_deadline' => $this->faker->date('Y-m-d H:i:s'),
        'tags' => $this->faker->text,
        'is_garentie' => $this->faker->word,
        'duree_garentie' => $this->faker->randomDigitNotNull,
        'is_gain_shop' => $this->faker->word,
        'is_exclusive_price' => $this->faker->word,
        'is_promo_gain_shop' => $this->faker->word,
        'is_selling' => $this->faker->word,
        'promo_gain_shop' => $this->faker->word,
        'promo_gain_shop_price' => $this->faker->word,
        'is_night_market' => $this->faker->word,
        'promo_night_market_price' => $this->faker->word,
        'promo_night_market_percentage' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
