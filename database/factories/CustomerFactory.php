<?php

namespace Database\Factories;

use App\Models\Customer;
use Illuminate\Database\Eloquent\Factories\Factory;

class CustomerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Customer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'first_name' => $this->faker->word,
        'last_name' => $this->faker->word,
        'name' => $this->faker->word,
        'phone_number' => $this->faker->word,
        'phone' => $this->faker->word,
        'is_phone_verified' => $this->faker->word,
        'phone_verified_at' => $this->faker->date('Y-m-d H:i:s'),
        'is_active' => $this->faker->word,
        'activation_date' => $this->faker->date('Y-m-d H:i:s'),
        'dial_code' => $this->faker->word,
        'country_code' => $this->faker->word,
        'email' => $this->faker->word,
        'email_verified_at' => $this->faker->date('Y-m-d H:i:s'),
        'photo_id' => $this->faker->word,
        'photo_url' => $this->faker->word,
        'photo_mime' => $this->faker->word,
        'photo_height' => $this->faker->word,
        'photo_width' => $this->faker->word,
        'photo_length' => $this->faker->word,
        'gender' => $this->faker->word,
        'bio' => $this->faker->word,
        'birthday' => $this->faker->word,
        'birth_location' => $this->faker->word,
        'language' => $this->faker->word,
        'facebook_id' => $this->faker->word,
        'twitter_id' => $this->faker->word,
        'google_id' => $this->faker->word,
        'is_pro_seller' => $this->faker->word,
        'has_adjemin_pay' => $this->faker->word,
        'origin' => $this->faker->word,
        'password' => $this->faker->word,
        'wallet_password' => $this->faker->word,
        'is_wallet_active' => $this->faker->word,
        'wallet_activation_date' => $this->faker->date('Y-m-d H:i:s'),
        'default_currency' => $this->faker->word,
        'identity_card_ref' => $this->faker->word,
        'identity_card_link_recto' => $this->faker->word,
        'identity_card_link_verso' => $this->faker->word,
        'identity_card_type' => $this->faker->word,
        'account_type' => $this->faker->word,
        'company_name' => $this->faker->word,
        'company_country' => $this->faker->word,
        'company_reference' => $this->faker->word,
        'last_location_gps' => $this->faker->word,
        'rating' => $this->faker->randomDigitNotNull,
        'username' => $this->faker->word,
        'godfather_username' => $this->faker->word,
        'has_badge' => $this->faker->word,
        'is_email_registration' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
