<?php

namespace Database\Factories;

use App\Models\Order;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'customer_id' => $this->faker->word,
        'seller_id' => $this->faker->word,
        'seller_type' => $this->faker->word,
        'is_waiting' => $this->faker->word,
        'current_status' => $this->faker->word,
        'rating' => $this->faker->word,
        'notes' => $this->faker->text,
        'note_description' => $this->faker->text,
        'note_date' => $this->faker->date('Y-m-d H:i:s'),
        'reports' => $this->faker->text,
        'report_description' => $this->faker->text,
        'report_date' => $this->faker->date('Y-m-d H:i:s'),
        'payment_method_slug' => $this->faker->word,
        'delivery_formula_slug' => $this->faker->word,
        'delivery_fees' => $this->faker->word,
        'amount' => $this->faker->word,
        'currency_code' => $this->faker->word,
        'is_delivered' => $this->faker->word,
        'is_waiting_payment' => $this->faker->word,
        'is_waiting_note' => $this->faker->word,
        'delivery_date' => $this->faker->date('Y-m-d H:i:s'),
        'task_id' => $this->faker->word,
        'merchant_assignments' => $this->faker->text,
        'merchant_assignment_accepted' => $this->faker->text,
        'reference' => $this->faker->word,
        'seller_has_delivred' => $this->faker->word,
        'seller_has_confirmed' => $this->faker->word,
        'customer_has_confirmed_delivery' => $this->faker->word,
        'customer_delivery_signature' => $this->faker->word,
        'customer_delivery_signature_at' => $this->faker->date('Y-m-d H:i:s'),
        'is_customer_reported' => $this->faker->word,
        'is_completed' => $this->faker->word,
        'is_seller_paid' => $this->faker->word,
        'has_litige' => $this->faker->word,
        'is_refunded' => $this->faker->word,
        'is_litige_solved' => $this->faker->word,
        'litige_solution' => $this->faker->word,
        'is_returned' => $this->faker->word,
        'is_customer_satisfied' => $this->faker->word,
        'warranty_period_days' => $this->faker->randomDigitNotNull,
        'warranty_period_last_date' => $this->faker->date('Y-m-d H:i:s'),
        'return_delivery_signature' => $this->faker->word,
        'return_delivery_signature_at' => $this->faker->date('Y-m-d H:i:s'),
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
