<?php

namespace Database\Factories;

use App\Models\OrderLitige;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderLitigeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = OrderLitige::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'order_id' => $this->faker->word,
        'customer_id' => $this->faker->word,
        'seller_id' => $this->faker->word,
        'customer_solution' => $this->faker->word,
        'seller_solution' => $this->faker->word,
        'seller_note' => $this->faker->text,
        'final_litige_solution' => $this->faker->word,
        'litige_accepted' => $this->faker->word,
        'feedback_id' => $this->faker->word,
        'litige_rejected_reason' => $this->faker->text,
        'solved' => $this->faker->word,
        'is_completed' => $this->faker->word,
        'is_waiting_admin_acceptation' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
