<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/','/login');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::resource('customers', App\Http\Controllers\CustomerController::class);

Route::resource('products', App\Http\Controllers\ProductController::class);

Route::resource('orders', App\Http\Controllers\OrderController::class);

Route::resource('boutiques', App\Http\Controllers\BoutiqueController::class);

Route::resource('orderLitiges', App\Http\Controllers\OrderLitigeController::class);
Route::get('orderLitiges/rejected/{id}', [App\Http\Controllers\OrderLitigeController::class, 'rejected'])->name('orderLitiges.rejected');
Route::get('orderLitiges/approved/{id}', [App\Http\Controllers\OrderLitigeController::class, 'approved'])->name('orderLitiges.approved');
Route::get('orderLitiges/solved/{id}', [App\Http\Controllers\OrderLitigeController::class, 'solved'])->name('orderLitiges.solved');
