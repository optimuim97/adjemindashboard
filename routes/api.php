<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::resource('customers', App\Http\Controllers\API\CustomerAPIController::class);

Route::resource('products', App\Http\Controllers\API\ProductAPIController::class);

Route::resource('orders', App\Http\Controllers\API\OrderAPIController::class);




Route::resource('boutiques', App\Http\Controllers\API\BoutiqueAPIController::class);


Route::resource('order_litiges', App\Http\Controllers\API\OrderLitigeAPIController::class);
