<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $product->title }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $product->description }}</p>
</div>

<!-- Description Metadata Field -->
<div class="form-group">
    {!! Form::label('description_metadata', 'Description Metadata:') !!}
    <p>{{ $product->description_metadata }}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{{ $product->price }}</p>
</div>

<!-- Original Price Field -->
<div class="form-group">
    {!! Form::label('original_price', 'Original Price:') !!}
    <p>{{ $product->original_price }}</p>
</div>

<!-- Fees Field -->
<div class="form-group">
    {!! Form::label('fees', 'Fees:') !!}
    <p>{{ $product->fees }}</p>
</div>

<!-- Currency Slug Field -->
<div class="form-group">
    {!! Form::label('currency_slug', 'Currency Slug:') !!}
    <p>{{ $product->currency_slug }}</p>
</div>

<!-- Old Price Field -->
<div class="form-group">
    {!! Form::label('old_price', 'Old Price:') !!}
    <p>{{ $product->old_price }}</p>
</div>

<!-- Has Promo Field -->
<div class="form-group">
    {!! Form::label('has_promo', 'Has Promo:') !!}
    <p>{{ $product->has_promo }}</p>
</div>

<!-- Promo Percentage Field -->
<div class="form-group">
    {!! Form::label('promo_percentage', 'Promo Percentage:') !!}
    <p>{{ $product->promo_percentage }}</p>
</div>

<!-- Medias Field -->
<div class="form-group">
    {!! Form::label('medias', 'Medias:') !!}
    <p>{{ $product->medias }}</p>
</div>

<!-- Cover Id Field -->
<div class="form-group">
    {!! Form::label('cover_id', 'Cover Id:') !!}
    <p>{{ $product->cover_id }}</p>
</div>

<!-- Cover Type Field -->
<div class="form-group">
    {!! Form::label('cover_type', 'Cover Type:') !!}
    <p>{{ $product->cover_type }}</p>
</div>

<!-- Cover Url Field -->
<div class="form-group">
    {!! Form::label('cover_url', 'Cover Url:') !!}
    <p>{{ $product->cover_url }}</p>
</div>

<!-- Cover Thumbnail Field -->
<div class="form-group">
    {!! Form::label('cover_thumbnail', 'Cover Thumbnail:') !!}
    <p>{{ $product->cover_thumbnail }}</p>
</div>

<!-- Cover Duration Field -->
<div class="form-group">
    {!! Form::label('cover_duration', 'Cover Duration:') !!}
    <p>{{ $product->cover_duration }}</p>
</div>

<!-- Cover Width Field -->
<div class="form-group">
    {!! Form::label('cover_width', 'Cover Width:') !!}
    <p>{{ $product->cover_width }}</p>
</div>

<!-- Cover Height Field -->
<div class="form-group">
    {!! Form::label('cover_height', 'Cover Height:') !!}
    <p>{{ $product->cover_height }}</p>
</div>

<!-- Cover Blurhash Field -->
<div class="form-group">
    {!! Form::label('cover_blurhash', 'Cover Blurhash:') !!}
    <p>{{ $product->cover_blurhash }}</p>
</div>

<!-- Location Name Field -->
<div class="form-group">
    {!! Form::label('location_name', 'Location Name:') !!}
    <p>{{ $product->location_name }}</p>
</div>

<!-- Location Address Field -->
<div class="form-group">
    {!! Form::label('location_address', 'Location Address:') !!}
    <p>{{ $product->location_address }}</p>
</div>

<!-- Location Lat Field -->
<div class="form-group">
    {!! Form::label('location_lat', 'Location Lat:') !!}
    <p>{{ $product->location_lat }}</p>
</div>

<!-- Location Lng Field -->
<div class="form-group">
    {!! Form::label('location_lng', 'Location Lng:') !!}
    <p>{{ $product->location_lng }}</p>
</div>

<!-- Category Id Field -->
<div class="form-group">
    {!! Form::label('category_id', 'Category Id:') !!}
    <p>{{ $product->category_id }}</p>
</div>

<!-- Category Meta Field -->
<div class="form-group">
    {!! Form::label('category_meta', 'Category Meta:') !!}
    <p>{{ $product->category_meta }}</p>
</div>

<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    <p>{{ $product->customer_id }}</p>
</div>

<!-- Is From Shop Field -->
<div class="form-group">
    {!! Form::label('is_from_shop', 'Is From Shop:') !!}
    <p>{{ $product->is_from_shop }}</p>
</div>

<!-- Is Sold Field -->
<div class="form-group">
    {!! Form::label('is_sold', 'Is Sold:') !!}
    <p>{{ $product->is_sold }}</p>
</div>

<!-- Sold At Field -->
<div class="form-group">
    {!! Form::label('sold_at', 'Sold At:') !!}
    <p>{{ $product->sold_at }}</p>
</div>

<!-- Initiale Count Field -->
<div class="form-group">
    {!! Form::label('initiale_count', 'Initiale Count:') !!}
    <p>{{ $product->initiale_count }}</p>
</div>

<!-- Is Firm Price Field -->
<div class="form-group">
    {!! Form::label('is_firm_price', 'Is Firm Price:') !!}
    <p>{{ $product->is_firm_price }}</p>
</div>

<!-- Condition Id Field -->
<div class="form-group">
    {!! Form::label('condition_id', 'Condition Id:') !!}
    <p>{{ $product->condition_id }}</p>
</div>

<!-- Published At Field -->
<div class="form-group">
    {!! Form::label('published_at', 'Published At:') !!}
    <p>{{ $product->published_at }}</p>
</div>

<!-- Is Published Field -->
<div class="form-group">
    {!! Form::label('is_published', 'Is Published:') !!}
    <p>{{ $product->is_published }}</p>
</div>

<!-- Deleted By Field -->
<div class="form-group">
    {!! Form::label('deleted_by', 'Deleted By:') !!}
    <p>{{ $product->deleted_by }}</p>
</div>

<!-- Deleted Creator Field -->
<div class="form-group">
    {!! Form::label('deleted_creator', 'Deleted Creator:') !!}
    <p>{{ $product->deleted_creator }}</p>
</div>

<!-- Sold Count Field -->
<div class="form-group">
    {!! Form::label('sold_count', 'Sold Count:') !!}
    <p>{{ $product->sold_count }}</p>
</div>

<!-- Delivery Services Field -->
<div class="form-group">
    {!! Form::label('delivery_services', 'Delivery Services:') !!}
    <p>{{ $product->delivery_services }}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    <p>{{ $product->slug }}</p>
</div>

<!-- Link Field -->
<div class="form-group">
    {!! Form::label('link', 'Link:') !!}
    <p>{{ $product->link }}</p>
</div>

<!-- Has Delivery Field -->
<div class="form-group">
    {!! Form::label('has_delivery', 'Has Delivery:') !!}
    <p>{{ $product->has_delivery }}</p>
</div>

<!-- Has Ordering Field -->
<div class="form-group">
    {!! Form::label('has_ordering', 'Has Ordering:') !!}
    <p>{{ $product->has_ordering }}</p>
</div>

<!-- Order Payment Methods Field -->
<div class="form-group">
    {!! Form::label('order_payment_methods', 'Order Payment Methods:') !!}
    <p>{{ $product->order_payment_methods }}</p>
</div>

<!-- Visibility Deadline Field -->
<div class="form-group">
    {!! Form::label('visibility_deadline', 'Visibility Deadline:') !!}
    <p>{{ $product->visibility_deadline }}</p>
</div>

<!-- Tags Field -->
<div class="form-group">
    {!! Form::label('tags', 'Tags:') !!}
    <p>{{ $product->tags }}</p>
</div>

<!-- Is Garentie Field -->
<div class="form-group">
    {!! Form::label('is_garentie', 'Is Garentie:') !!}
    <p>{{ $product->is_garentie }}</p>
</div>

<!-- Duree Garentie Field -->
<div class="form-group">
    {!! Form::label('duree_garentie', 'Duree Garentie:') !!}
    <p>{{ $product->duree_garentie }}</p>
</div>

<!-- Is Gain Shop Field -->
<div class="form-group">
    {!! Form::label('is_gain_shop', 'Is Gain Shop:') !!}
    <p>{{ $product->is_gain_shop }}</p>
</div>

<!-- Is Exclusive Price Field -->
<div class="form-group">
    {!! Form::label('is_exclusive_price', 'Is Exclusive Price:') !!}
    <p>{{ $product->is_exclusive_price }}</p>
</div>

<!-- Is Promo Gain Shop Field -->
<div class="form-group">
    {!! Form::label('is_promo_gain_shop', 'Is Promo Gain Shop:') !!}
    <p>{{ $product->is_promo_gain_shop }}</p>
</div>

<!-- Is Selling Field -->
<div class="form-group">
    {!! Form::label('is_selling', 'Is Selling:') !!}
    <p>{{ $product->is_selling }}</p>
</div>

<!-- Promo Gain Shop Field -->
<div class="form-group">
    {!! Form::label('promo_gain_shop', 'Promo Gain Shop:') !!}
    <p>{{ $product->promo_gain_shop }}</p>
</div>

<!-- Promo Gain Shop Price Field -->
<div class="form-group">
    {!! Form::label('promo_gain_shop_price', 'Promo Gain Shop Price:') !!}
    <p>{{ $product->promo_gain_shop_price }}</p>
</div>

<!-- Is Night Market Field -->
<div class="form-group">
    {!! Form::label('is_night_market', 'Is Night Market:') !!}
    <p>{{ $product->is_night_market }}</p>
</div>

<!-- Promo Night Market Price Field -->
<div class="form-group">
    {!! Form::label('promo_night_market_price', 'Promo Night Market Price:') !!}
    <p>{{ $product->promo_night_market_price }}</p>
</div>

<!-- Promo Night Market Percentage Field -->
<div class="form-group">
    {!! Form::label('promo_night_market_percentage', 'Promo Night Market Percentage:') !!}
    <p>{{ $product->promo_night_market_percentage }}</p>
</div>

