<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Metadata Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description_metadata', 'Description Metadata:') !!}
    {!! Form::textarea('description_metadata', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'Price:') !!}
    {!! Form::text('price', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Original Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('original_price', 'Original Price:') !!}
    {!! Form::text('original_price', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Fees Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fees', 'Fees:') !!}
    {!! Form::text('fees', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Currency Slug Field -->
<div class="form-group col-sm-6">
    {!! Form::label('currency_slug', 'Currency Slug:') !!}
    {!! Form::text('currency_slug', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Old Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('old_price', 'Old Price:') !!}
    {!! Form::text('old_price', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Has Promo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('has_promo', 'Has Promo:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('has_promo', 0) !!}
        {!! Form::checkbox('has_promo', '1', null) !!}
    </label>
</div>


<!-- Promo Percentage Field -->
<div class="form-group col-sm-6">
    {!! Form::label('promo_percentage', 'Promo Percentage:') !!}
    {!! Form::text('promo_percentage', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Medias Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('medias', 'Medias:') !!}
    {!! Form::textarea('medias', null, ['class' => 'form-control']) !!}
</div>

<!-- Cover Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cover_id', 'Cover Id:') !!}
    {!! Form::number('cover_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Cover Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cover_type', 'Cover Type:') !!}
    {!! Form::text('cover_type', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Cover Url Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cover_url', 'Cover Url:') !!}
    {!! Form::text('cover_url', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Cover Thumbnail Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cover_thumbnail', 'Cover Thumbnail:') !!}
    {!! Form::text('cover_thumbnail', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Cover Duration Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cover_duration', 'Cover Duration:') !!}
    {!! Form::text('cover_duration', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Cover Width Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cover_width', 'Cover Width:') !!}
    {!! Form::text('cover_width', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Cover Height Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cover_height', 'Cover Height:') !!}
    {!! Form::text('cover_height', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Cover Blurhash Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cover_blurhash', 'Cover Blurhash:') !!}
    {!! Form::text('cover_blurhash', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Location Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location_name', 'Location Name:') !!}
    {!! Form::text('location_name', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Location Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location_address', 'Location Address:') !!}
    {!! Form::text('location_address', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Location Lat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location_lat', 'Location Lat:') !!}
    {!! Form::number('location_lat', null, ['class' => 'form-control']) !!}
</div>

<!-- Location Lng Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location_lng', 'Location Lng:') !!}
    {!! Form::number('location_lng', null, ['class' => 'form-control']) !!}
</div>

<!-- Category Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('category_id', 'Category Id:') !!}
    {!! Form::number('category_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Category Meta Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('category_meta', 'Category Meta:') !!}
    {!! Form::textarea('category_meta', null, ['class' => 'form-control']) !!}
</div>

<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    {!! Form::number('customer_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Is From Shop Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_from_shop', 'Is From Shop:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_from_shop', 0) !!}
        {!! Form::checkbox('is_from_shop', '1', null) !!}
    </label>
</div>


<!-- Is Sold Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_sold', 'Is Sold:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_sold', 0) !!}
        {!! Form::checkbox('is_sold', '1', null) !!}
    </label>
</div>


<!-- Sold At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sold_at', 'Sold At:') !!}
    {!! Form::text('sold_at', null, ['class' => 'form-control','id'=>'sold_at']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#sold_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Initiale Count Field -->
<div class="form-group col-sm-6">
    {!! Form::label('initiale_count', 'Initiale Count:') !!}
    {!! Form::text('initiale_count', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Is Firm Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_firm_price', 'Is Firm Price:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_firm_price', 0) !!}
        {!! Form::checkbox('is_firm_price', '1', null) !!}
    </label>
</div>


<!-- Condition Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('condition_id', 'Condition Id:') !!}
    {!! Form::number('condition_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Published At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('published_at', 'Published At:') !!}
    {!! Form::text('published_at', null, ['class' => 'form-control','id'=>'published_at']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#published_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Is Published Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_published', 'Is Published:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_published', 0) !!}
        {!! Form::checkbox('is_published', '1', null) !!}
    </label>
</div>


<!-- Deleted By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('deleted_by', 'Deleted By:') !!}
    {!! Form::number('deleted_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Deleted Creator Field -->
<div class="form-group col-sm-6">
    {!! Form::label('deleted_creator', 'Deleted Creator:') !!}
    {!! Form::text('deleted_creator', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Sold Count Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sold_count', 'Sold Count:') !!}
    {!! Form::text('sold_count', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Delivery Services Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('delivery_services', 'Delivery Services:') !!}
    {!! Form::textarea('delivery_services', null, ['class' => 'form-control']) !!}
</div>

<!-- Slug Field -->
<div class="form-group col-sm-6">
    {!! Form::label('slug', 'Slug:') !!}
    {!! Form::text('slug', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Link Field -->
<div class="form-group col-sm-6">
    {!! Form::label('link', 'Link:') !!}
    {!! Form::text('link', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Has Delivery Field -->
<div class="form-group col-sm-6">
    {!! Form::label('has_delivery', 'Has Delivery:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('has_delivery', 0) !!}
        {!! Form::checkbox('has_delivery', '1', null) !!}
    </label>
</div>


<!-- Has Ordering Field -->
<div class="form-group col-sm-6">
    {!! Form::label('has_ordering', 'Has Ordering:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('has_ordering', 0) !!}
        {!! Form::checkbox('has_ordering', '1', null) !!}
    </label>
</div>


<!-- Order Payment Methods Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('order_payment_methods', 'Order Payment Methods:') !!}
    {!! Form::textarea('order_payment_methods', null, ['class' => 'form-control']) !!}
</div>

<!-- Visibility Deadline Field -->
<div class="form-group col-sm-6">
    {!! Form::label('visibility_deadline', 'Visibility Deadline:') !!}
    {!! Form::text('visibility_deadline', null, ['class' => 'form-control','id'=>'visibility_deadline']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#visibility_deadline').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Tags Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('tags', 'Tags:') !!}
    {!! Form::textarea('tags', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Garentie Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_garentie', 'Is Garentie:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_garentie', 0) !!}
        {!! Form::checkbox('is_garentie', '1', null) !!}
    </label>
</div>


<!-- Duree Garentie Field -->
<div class="form-group col-sm-6">
    {!! Form::label('duree_garentie', 'Duree Garentie:') !!}
    {!! Form::number('duree_garentie', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Gain Shop Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_gain_shop', 'Is Gain Shop:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_gain_shop', 0) !!}
        {!! Form::checkbox('is_gain_shop', '1', null) !!}
    </label>
</div>


<!-- Is Exclusive Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_exclusive_price', 'Is Exclusive Price:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_exclusive_price', 0) !!}
        {!! Form::checkbox('is_exclusive_price', '1', null) !!}
    </label>
</div>


<!-- Is Promo Gain Shop Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_promo_gain_shop', 'Is Promo Gain Shop:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_promo_gain_shop', 0) !!}
        {!! Form::checkbox('is_promo_gain_shop', '1', null) !!}
    </label>
</div>


<!-- Is Selling Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_selling', 'Is Selling:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_selling', 0) !!}
        {!! Form::checkbox('is_selling', '1', null) !!}
    </label>
</div>


<!-- Promo Gain Shop Field -->
<div class="form-group col-sm-6">
    {!! Form::label('promo_gain_shop', 'Promo Gain Shop:') !!}
    {!! Form::text('promo_gain_shop', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Promo Gain Shop Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('promo_gain_shop_price', 'Promo Gain Shop Price:') !!}
    {!! Form::text('promo_gain_shop_price', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Is Night Market Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_night_market', 'Is Night Market:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_night_market', 0) !!}
        {!! Form::checkbox('is_night_market', '1', null) !!}
    </label>
</div>


<!-- Promo Night Market Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('promo_night_market_price', 'Promo Night Market Price:') !!}
    {!! Form::text('promo_night_market_price', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Promo Night Market Percentage Field -->
<div class="form-group col-sm-6">
    {!! Form::label('promo_night_market_percentage', 'Promo Night Market Percentage:') !!}
    {!! Form::text('promo_night_market_percentage', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('products.index') }}" class="btn btn-light">Cancel</a>
</div>
