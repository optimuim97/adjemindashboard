<li class="side-menus {{ Request::is('*') ? 'active' : '' }}">
    <a class="nav-link" href="/home">
        <i class=" fas fa-building"></i><span>Tableau de Bord</span>
    </a>
</li>


<li class="side-menus {{ Request::is('customers*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('customers.index') }}"><i class="fas fa-building"></i><span>Utilisateurs</span></a>
</li>

<li class="side-menus {{ Request::is('products*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('products.index') }}"><i class="fas fa-building"></i><span>Produits</span></a>
</li>

<li class="side-menus {{ Request::is('orders*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('orders.index') }}"><i class="fas fa-building"></i><span>Commande</span></a>
</li>

<li class="side-menus {{ Request::is('orderLitiges*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('orderLitiges.index') }}"><i class="fas fa-building"></i><span>Gestion de litiges</span></a>
</li>

<li class="side-menus {{ Request::is('boutiques*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('boutiques.index') }}"><i class="fas fa-building"></i><span>Boutiques</span></a>
</li>


