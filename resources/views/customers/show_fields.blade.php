<!-- First Name Field -->
<div class="form-group">
    {!! Form::label('first_name', 'First Name:') !!}
    <p>{{ $customer->first_name }}</p>
</div>

<!-- Last Name Field -->
<div class="form-group">
    {!! Form::label('last_name', 'Last Name:') !!}
    <p>{{ $customer->last_name }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $customer->name }}</p>
</div>

<!-- Phone Number Field -->
<div class="form-group">
    {!! Form::label('phone_number', 'Phone Number:') !!}
    <p>{{ $customer->phone_number }}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{{ $customer->phone }}</p>
</div>

<!-- Is Phone Verified Field -->
<div class="form-group">
    {!! Form::label('is_phone_verified', 'Is Phone Verified:') !!}
    <p>{{ $customer->is_phone_verified }}</p>
</div>

<!-- Phone Verified At Field -->
<div class="form-group">
    {!! Form::label('phone_verified_at', 'Phone Verified At:') !!}
    <p>{{ $customer->phone_verified_at }}</p>
</div>

<!-- Is Active Field -->
<div class="form-group">
    {!! Form::label('is_active', 'Is Active:') !!}
    <p>{{ $customer->is_active }}</p>
</div>

<!-- Activation Date Field -->
<div class="form-group">
    {!! Form::label('activation_date', 'Activation Date:') !!}
    <p>{{ $customer->activation_date }}</p>
</div>

<!-- Dial Code Field -->
<div class="form-group">
    {!! Form::label('dial_code', 'Dial Code:') !!}
    <p>{{ $customer->dial_code }}</p>
</div>

<!-- Country Code Field -->
<div class="form-group">
    {!! Form::label('country_code', 'Country Code:') !!}
    <p>{{ $customer->country_code }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $customer->email }}</p>
</div>

<!-- Email Verified At Field -->
<div class="form-group">
    {!! Form::label('email_verified_at', 'Email Verified At:') !!}
    <p>{{ $customer->email_verified_at }}</p>
</div>

<!-- Photo Id Field -->
<div class="form-group">
    {!! Form::label('photo_id', 'Photo Id:') !!}
    <p>{{ $customer->photo_id }}</p>
</div>

<!-- Photo Url Field -->
<div class="form-group">
    {!! Form::label('photo_url', 'Photo Url:') !!}
    <p>{{ $customer->photo_url }}</p>
</div>

<!-- Photo Mime Field -->
<div class="form-group">
    {!! Form::label('photo_mime', 'Photo Mime:') !!}
    <p>{{ $customer->photo_mime }}</p>
</div>

<!-- Photo Height Field -->
<div class="form-group">
    {!! Form::label('photo_height', 'Photo Height:') !!}
    <p>{{ $customer->photo_height }}</p>
</div>

<!-- Photo Width Field -->
<div class="form-group">
    {!! Form::label('photo_width', 'Photo Width:') !!}
    <p>{{ $customer->photo_width }}</p>
</div>

<!-- Photo Length Field -->
<div class="form-group">
    {!! Form::label('photo_length', 'Photo Length:') !!}
    <p>{{ $customer->photo_length }}</p>
</div>

<!-- Gender Field -->
<div class="form-group">
    {!! Form::label('gender', 'Gender:') !!}
    <p>{{ $customer->gender }}</p>
</div>

<!-- Bio Field -->
<div class="form-group">
    {!! Form::label('bio', 'Bio:') !!}
    <p>{{ $customer->bio }}</p>
</div>

<!-- Birthday Field -->
<div class="form-group">
    {!! Form::label('birthday', 'Birthday:') !!}
    <p>{{ $customer->birthday }}</p>
</div>

<!-- Birth Location Field -->
<div class="form-group">
    {!! Form::label('birth_location', 'Birth Location:') !!}
    <p>{{ $customer->birth_location }}</p>
</div>

<!-- Language Field -->
<div class="form-group">
    {!! Form::label('language', 'Language:') !!}
    <p>{{ $customer->language }}</p>
</div>

<!-- Facebook Id Field -->
<div class="form-group">
    {!! Form::label('facebook_id', 'Facebook Id:') !!}
    <p>{{ $customer->facebook_id }}</p>
</div>

<!-- Twitter Id Field -->
<div class="form-group">
    {!! Form::label('twitter_id', 'Twitter Id:') !!}
    <p>{{ $customer->twitter_id }}</p>
</div>

<!-- Google Id Field -->
<div class="form-group">
    {!! Form::label('google_id', 'Google Id:') !!}
    <p>{{ $customer->google_id }}</p>
</div>

<!-- Is Pro Seller Field -->
<div class="form-group">
    {!! Form::label('is_pro_seller', 'Is Pro Seller:') !!}
    <p>{{ $customer->is_pro_seller }}</p>
</div>

<!-- Has Adjemin Pay Field -->
<div class="form-group">
    {!! Form::label('has_adjemin_pay', 'Has Adjemin Pay:') !!}
    <p>{{ $customer->has_adjemin_pay }}</p>
</div>

<!-- Origin Field -->
<div class="form-group">
    {!! Form::label('origin', 'Origin:') !!}
    <p>{{ $customer->origin }}</p>
</div>

<!-- Password Field -->
<div class="form-group">
    {!! Form::label('password', 'Password:') !!}
    <p>{{ $customer->password }}</p>
</div>

<!-- Wallet Password Field -->
<div class="form-group">
    {!! Form::label('wallet_password', 'Wallet Password:') !!}
    <p>{{ $customer->wallet_password }}</p>
</div>

<!-- Is Wallet Active Field -->
<div class="form-group">
    {!! Form::label('is_wallet_active', 'Is Wallet Active:') !!}
    <p>{{ $customer->is_wallet_active }}</p>
</div>

<!-- Wallet Activation Date Field -->
<div class="form-group">
    {!! Form::label('wallet_activation_date', 'Wallet Activation Date:') !!}
    <p>{{ $customer->wallet_activation_date }}</p>
</div>

<!-- Default Currency Field -->
<div class="form-group">
    {!! Form::label('default_currency', 'Default Currency:') !!}
    <p>{{ $customer->default_currency }}</p>
</div>

<!-- Identity Card Ref Field -->
<div class="form-group">
    {!! Form::label('identity_card_ref', 'Identity Card Ref:') !!}
    <p>{{ $customer->identity_card_ref }}</p>
</div>

<!-- Identity Card Link Recto Field -->
<div class="form-group">
    {!! Form::label('identity_card_link_recto', 'Identity Card Link Recto:') !!}
    <p>{{ $customer->identity_card_link_recto }}</p>
</div>

<!-- Identity Card Link Verso Field -->
<div class="form-group">
    {!! Form::label('identity_card_link_verso', 'Identity Card Link Verso:') !!}
    <p>{{ $customer->identity_card_link_verso }}</p>
</div>

<!-- Identity Card Type Field -->
<div class="form-group">
    {!! Form::label('identity_card_type', 'Identity Card Type:') !!}
    <p>{{ $customer->identity_card_type }}</p>
</div>

<!-- Account Type Field -->
<div class="form-group">
    {!! Form::label('account_type', 'Account Type:') !!}
    <p>{{ $customer->account_type }}</p>
</div>

<!-- Company Name Field -->
<div class="form-group">
    {!! Form::label('company_name', 'Company Name:') !!}
    <p>{{ $customer->company_name }}</p>
</div>

<!-- Company Country Field -->
<div class="form-group">
    {!! Form::label('company_country', 'Company Country:') !!}
    <p>{{ $customer->company_country }}</p>
</div>

<!-- Company Reference Field -->
<div class="form-group">
    {!! Form::label('company_reference', 'Company Reference:') !!}
    <p>{{ $customer->company_reference }}</p>
</div>

<!-- Last Location Gps Field -->
<div class="form-group">
    {!! Form::label('last_location_gps', 'Last Location Gps:') !!}
    <p>{{ $customer->last_location_gps }}</p>
</div>

<!-- Rating Field -->
<div class="form-group">
    {!! Form::label('rating', 'Rating:') !!}
    <p>{{ $customer->rating }}</p>
</div>

<!-- Username Field -->
<div class="form-group">
    {!! Form::label('username', 'Username:') !!}
    <p>{{ $customer->username }}</p>
</div>

<!-- Godfather Username Field -->
<div class="form-group">
    {!! Form::label('godfather_username', 'Godfather Username:') !!}
    <p>{{ $customer->godfather_username }}</p>
</div>

<!-- Has Badge Field -->
<div class="form-group">
    {!! Form::label('has_badge', 'Has Badge:') !!}
    <p>{{ $customer->has_badge }}</p>
</div>

<!-- Is Email Registration Field -->
<div class="form-group">
    {!! Form::label('is_email_registration', 'Is Email Registration:') !!}
    <p>{{ $customer->is_email_registration }}</p>
</div>

