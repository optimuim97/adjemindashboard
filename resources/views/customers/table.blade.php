<table class="table table-responsive-sm table-striped table-bordered" id="customersTable">
    <thead>
    <tr>
        <th scope="col">first_name</th>
        <th scope="col">last_name</th>
        <th scope="col">name</th>
        <th scope="col">phone_number</th>
        <th scope="col">phone</th>
        <th scope="col">is_phone_verified</th>
        <th scope="col">phone_verified_at</th><th scope="col">is_active</th><th scope="col">activation_date</th><th scope="col">dial_code</th><th scope="col">country_code</th><th scope="col">email</th><th scope="col">email_verified_at</th><th scope="col">photo_id</th><th scope="col">photo_url</th><th scope="col">photo_mime</th><th scope="col">photo_height</th><th scope="col">photo_width</th><th scope="col">photo_length</th><th scope="col">gender</th><th scope="col">bio</th><th scope="col">birthday</th><th scope="col">birth_location</th><th scope="col">language</th><th scope="col">facebook_id</th><th scope="col">twitter_id</th><th scope="col">google_id</th><th scope="col">is_pro_seller</th><th scope="col">has_adjemin_pay</th><th scope="col">origin</th><th scope="col">password</th><th scope="col">wallet_password</th><th scope="col">is_wallet_active</th><th scope="col">wallet_activation_date</th><th scope="col">default_currency</th><th scope="col">identity_card_ref</th><th scope="col">identity_card_link_recto</th><th scope="col">identity_card_link_verso</th><th scope="col">identity_card_type</th><th scope="col">account_type</th><th scope="col">company_name</th><th scope="col">company_country</th><th scope="col">company_reference</th><th scope="col">last_location_gps</th><th scope="col">rating</th><th scope="col">username</th><th scope="col">godfather_username</th><th scope="col">has_badge</th><th scope="col">is_email_registration</th>
        <th scope="col">Action</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
