<!-- First Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('first_name', 'First Name:') !!}
    {!! Form::text('first_name', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Last Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('last_name', 'Last Name:') !!}
    {!! Form::text('last_name', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Phone Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone_number', 'Phone Number:') !!}
    {!! Form::text('phone_number', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', 'Phone:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Is Phone Verified Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_phone_verified', 'Is Phone Verified:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_phone_verified', 0) !!}
        {!! Form::checkbox('is_phone_verified', '1', null) !!}
    </label>
</div>


<!-- Phone Verified At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone_verified_at', 'Phone Verified At:') !!}
    {!! Form::text('phone_verified_at', null, ['class' => 'form-control','id'=>'phone_verified_at']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#phone_verified_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Is Active Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_active', 'Is Active:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_active', 0) !!}
        {!! Form::checkbox('is_active', '1', null) !!}
    </label>
</div>


<!-- Activation Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('activation_date', 'Activation Date:') !!}
    {!! Form::text('activation_date', null, ['class' => 'form-control','id'=>'activation_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#activation_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Dial Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dial_code', 'Dial Code:') !!}
    {!! Form::text('dial_code', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Country Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('country_code', 'Country Code:') !!}
    {!! Form::text('country_code', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Email Verified At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email_verified_at', 'Email Verified At:') !!}
    {!! Form::text('email_verified_at', null, ['class' => 'form-control','id'=>'email_verified_at']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#email_verified_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Photo Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('photo_id', 'Photo Id:') !!}
    {!! Form::number('photo_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Photo Url Field -->
<div class="form-group col-sm-6">
    {!! Form::label('photo_url', 'Photo Url:') !!}
    {!! Form::text('photo_url', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Photo Mime Field -->
<div class="form-group col-sm-6">
    {!! Form::label('photo_mime', 'Photo Mime:') !!}
    {!! Form::text('photo_mime', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Photo Height Field -->
<div class="form-group col-sm-6">
    {!! Form::label('photo_height', 'Photo Height:') !!}
    {!! Form::text('photo_height', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Photo Width Field -->
<div class="form-group col-sm-6">
    {!! Form::label('photo_width', 'Photo Width:') !!}
    {!! Form::text('photo_width', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Photo Length Field -->
<div class="form-group col-sm-6">
    {!! Form::label('photo_length', 'Photo Length:') !!}
    {!! Form::text('photo_length', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Gender Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gender', 'Gender:') !!}
    {!! Form::text('gender', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Bio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bio', 'Bio:') !!}
    {!! Form::text('bio', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Birthday Field -->
<div class="form-group col-sm-6">
    {!! Form::label('birthday', 'Birthday:') !!}
    {!! Form::text('birthday', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Birth Location Field -->
<div class="form-group col-sm-6">
    {!! Form::label('birth_location', 'Birth Location:') !!}
    {!! Form::text('birth_location', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Language Field -->
<div class="form-group col-sm-6">
    {!! Form::label('language', 'Language:') !!}
    {!! Form::text('language', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Facebook Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('facebook_id', 'Facebook Id:') !!}
    {!! Form::text('facebook_id', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Twitter Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('twitter_id', 'Twitter Id:') !!}
    {!! Form::text('twitter_id', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Google Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('google_id', 'Google Id:') !!}
    {!! Form::text('google_id', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Is Pro Seller Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_pro_seller', 'Is Pro Seller:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_pro_seller', 0) !!}
        {!! Form::checkbox('is_pro_seller', '1', null) !!}
    </label>
</div>


<!-- Has Adjemin Pay Field -->
<div class="form-group col-sm-6">
    {!! Form::label('has_adjemin_pay', 'Has Adjemin Pay:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('has_adjemin_pay', 0) !!}
        {!! Form::checkbox('has_adjemin_pay', '1', null) !!}
    </label>
</div>


<!-- Origin Field -->
<div class="form-group col-sm-6">
    {!! Form::label('origin', 'Origin:') !!}
    {!! Form::text('origin', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', 'Password:') !!}
    {!! Form::password('password', ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Wallet Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('wallet_password', 'Wallet Password:') !!}
    {!! Form::text('wallet_password', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Is Wallet Active Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_wallet_active', 'Is Wallet Active:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_wallet_active', 0) !!}
        {!! Form::checkbox('is_wallet_active', '1', null) !!}
    </label>
</div>


<!-- Wallet Activation Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('wallet_activation_date', 'Wallet Activation Date:') !!}
    {!! Form::text('wallet_activation_date', null, ['class' => 'form-control','id'=>'wallet_activation_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#wallet_activation_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Default Currency Field -->
<div class="form-group col-sm-6">
    {!! Form::label('default_currency', 'Default Currency:') !!}
    {!! Form::text('default_currency', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Identity Card Ref Field -->
<div class="form-group col-sm-6">
    {!! Form::label('identity_card_ref', 'Identity Card Ref:') !!}
    {!! Form::text('identity_card_ref', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Identity Card Link Recto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('identity_card_link_recto', 'Identity Card Link Recto:') !!}
    {!! Form::text('identity_card_link_recto', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Identity Card Link Verso Field -->
<div class="form-group col-sm-6">
    {!! Form::label('identity_card_link_verso', 'Identity Card Link Verso:') !!}
    {!! Form::text('identity_card_link_verso', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Identity Card Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('identity_card_type', 'Identity Card Type:') !!}
    {!! Form::text('identity_card_type', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Account Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('account_type', 'Account Type:') !!}
    {!! Form::text('account_type', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Company Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('company_name', 'Company Name:') !!}
    {!! Form::text('company_name', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Company Country Field -->
<div class="form-group col-sm-6">
    {!! Form::label('company_country', 'Company Country:') !!}
    {!! Form::text('company_country', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Company Reference Field -->
<div class="form-group col-sm-6">
    {!! Form::label('company_reference', 'Company Reference:') !!}
    {!! Form::text('company_reference', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Last Location Gps Field -->
<div class="form-group col-sm-6">
    {!! Form::label('last_location_gps', 'Last Location Gps:') !!}
    {!! Form::text('last_location_gps', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Rating Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rating', 'Rating:') !!}
    {!! Form::number('rating', null, ['class' => 'form-control']) !!}
</div>

<!-- Username Field -->
<div class="form-group col-sm-6">
    {!! Form::label('username', 'Username:') !!}
    {!! Form::text('username', null, ['class' => 'form-control','maxlength' => 191,'maxlength' => 191,'maxlength' => 191]) !!}
</div>

<!-- Godfather Username Field -->
<div class="form-group col-sm-6">
    {!! Form::label('godfather_username', 'Godfather Username:') !!}
    {!! Form::text('godfather_username', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Has Badge Field -->
<div class="form-group col-sm-6">
    {!! Form::label('has_badge', 'Has Badge:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('has_badge', 0) !!}
        {!! Form::checkbox('has_badge', '1', null) !!}
    </label>
</div>


<!-- Is Email Registration Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_email_registration', 'Is Email Registration:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_email_registration', 0) !!}
        {!! Form::checkbox('is_email_registration', '1', null) !!}
    </label>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('customers.index') }}" class="btn btn-light">Cancel</a>
</div>
