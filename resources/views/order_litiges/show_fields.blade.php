
<div class="container">
    <div class="row">
        <!-- Order Id Field -->

      <!-- Order Id Field -->
        <div class="col-md-6">
            {!! Form::label('order_id', 'Numéro de la commande:') !!}
            <p>{{ $orderLitige->order_id }}</p>
        </div>

        <!-- Customer Id Field -->
        <div class="col-md-6">
            {!! Form::label('customer_id', 'Utilisteur:') !!}
            <p>{{ $orderLitige->customer ? $orderLitige->customer->name : 'donnée manquante' }}</p>
        </div>

        <!-- Seller Id Field -->
        <div class="col-md-6">
            {!! Form::label('seller_id', 'Vendeur :') !!}
            <p>{{ $orderLitige->seller ? $orderLitige->seller->name : 'donnée manquante' }}</p>
        </div>

        <!-- Customer Solution Field -->
        <div class="col-md-6">
            {!! Form::label('customer_solution', 'Solution de l\'utilisateur:') !!}
            <p>{{ $orderLitige->customer_solution }}</p>
        </div>

        <!-- Seller Solution Field -->
        <div class="col-md-6">
            {!! Form::label('seller_solution', 'Solution du vendeur:') !!}
            <p>{{ $orderLitige->seller_solution }}</p>
        </div>

        <!-- Seller Note Field -->
        <div class="col-md-6">
            {!! Form::label('seller_note', 'Remarque du venduer:') !!}
            <p>{{ $orderLitige->seller_note }}</p>
        </div>

        <!-- Final Litige Solution Field -->
        <div class="col-md-6">
            {!! Form::label('final_litige_solution', 'Solution final du litige:') !!}
            <p>{{ $orderLitige->final_litige_solution }}</p>
        </div>

        <!-- Litige Accepted Field -->
        <div class="col-md-6">
            {!! Form::label('litige_accepted', 'Status du Litige :') !!}
            <p>{{ $orderLitige->litige_accepted == true ? 'Oui': 'Non' }}</p>
        </div>

        <!-- Feedback Id Field -->
        <div class="col-md-6">
            {!! Form::label('feedback_id', 'Feedback Id:') !!}
            <p>{{ $orderLitige->feedback_id }}</p>
        </div>

        <!-- Litige Rejected Reason Field -->
        <div class="col-md-6">
            {!! Form::label('litige_rejected_reason', 'Raison de refus:') !!}
            <p>{{ $orderLitige->litige_rejected_reason }}</p>
        </div>

        <!-- Solved Field -->
        <div class="col-md-6">
            {!! Form::label('solved', 'Résolu ?:') !!}
            <p>{{ $orderLitige->solved == true ? 'Oui': 'Non'  }}</p>
        </div>

        <!-- Is Completed Field -->
        <div class="col-md-6">
            {!! Form::label('is_completed', 'Est terminé ?:') !!}
            <p>{{ $orderLitige->is_completed == true ? 'Oui': 'Non' }}</p>
        </div>

        <!-- Is Waiting Admin Acceptation Field -->
        <div class="col-md-6">
            {!! Form::label('is_waiting_admin_acceptation', 'En attente de d\'approbation:') !!}
            <p>{{ $orderLitige->is_waiting_admin_acceptation == true ? 'Oui': 'Non' }}</p>
        </div>
           <!-- Is Waiting Admin Acceptation Field -->
        <div class="col-md-6">
        </div>

        <div class="col-md-4">
            <button class="btn btn-primary">
                Définir la solutions du litige
            </button>
        </div>

        <div class="col-md-4">
            <button class="btn btn-success">
               Accepter
            </button>
        </div>

        <div class="col-md-4">
            <button class="btn btn-danger">
                Refuser
            </button>
        </div>

    </div>



</div>

@section('scripts')
    <script>

    </script>
@endsection
