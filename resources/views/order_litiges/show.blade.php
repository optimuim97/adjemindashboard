@extends('layouts.app')
@section('title')
    Details - Litiges
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
        <h1>Details - Litiges</h1>
        <div class="section-header-breadcrumb">
            <a href="{{ route('orderLitiges.index') }}"
                 class="btn btn-primary form-btn float-right">Retour</a>
        </div>
      </div>
   @include('stisla-templates::common.errors')
    <div class="section-body">
           <div class="card">
            <div class="card-body">
                    @include('order_litiges.show_fields')
            </div>
            </div>
    </div>
    </section>
@endsection
