<!-- Order Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('order_id', 'Order Id:') !!}
    {!! Form::number('order_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    {!! Form::number('customer_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Seller Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('seller_id', 'Seller Id:') !!}
    {!! Form::number('seller_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Customer Solution Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_solution', 'Customer Solution:') !!}
    {!! Form::text('customer_solution', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Seller Solution Field -->
<div class="form-group col-sm-6">
    {!! Form::label('seller_solution', 'Seller Solution:') !!}
    {!! Form::text('seller_solution', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Seller Note Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('seller_note', 'Seller Note:') !!}
    {!! Form::textarea('seller_note', null, ['class' => 'form-control']) !!}
</div>

<!-- Final Litige Solution Field -->
<div class="form-group col-sm-6">
    {!! Form::label('final_litige_solution', 'Final Litige Solution:') !!}
    {!! Form::text('final_litige_solution', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Litige Accepted Field -->
<div class="form-group col-sm-6">
    {!! Form::label('litige_accepted', 'Litige Accepted:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('litige_accepted', 0) !!}
        {!! Form::checkbox('litige_accepted', '1', null) !!}
    </label>
</div>


<!-- Feedback Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('feedback_id', 'Feedback Id:') !!}
    {!! Form::number('feedback_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Litige Rejected Reason Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('litige_rejected_reason', 'Litige Rejected Reason:') !!}
    {!! Form::textarea('litige_rejected_reason', null, ['class' => 'form-control']) !!}
</div>

<!-- Solved Field -->
<div class="form-group col-sm-6">
    {!! Form::label('solved', 'Solved:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('solved', 0) !!}
        {!! Form::checkbox('solved', '1', null) !!}
    </label>
</div>


<!-- Is Completed Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_completed', 'Is Completed:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_completed', 0) !!}
        {!! Form::checkbox('is_completed', '1', null) !!}
    </label>
</div>


<!-- Is Waiting Admin Acceptation Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_waiting_admin_acceptation', 'Is Waiting Admin Acceptation:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_waiting_admin_acceptation', 0) !!}
        {!! Form::checkbox('is_waiting_admin_acceptation', '1', null) !!}
    </label>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('orderLitiges.index') }}" class="btn btn-light">Cancel</a>
</div>
