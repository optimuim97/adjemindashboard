@extends('layouts.app')
@section('title')
    Gestions | Litiges
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Gestions | Litiges</h1>
            <div class="section-header-breadcrumb">
                <a href="{{ route('orderLitiges.create')}}" class="btn btn-primary form-btn">Gestions | Litiges <i class="fas fa-plus"></i></a>
            </div>
        </div>
    <div class="section-body">
       <div class="card">
            <div class="card-body">
                @include('order_litiges.table')
            </div>
       </div>
   </div>

    </section>
@endsection

