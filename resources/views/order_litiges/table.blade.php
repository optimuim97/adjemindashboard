<div class="table-responsive">
    <table class="table" id="orderLitiges-table">
        <thead>
            <tr>
                <th>#ID Numéro de la commande </th>
                <th>Utilisateurs </th>
                <th>Vendeur </th>
                <th>Solution de l'acheteur</th>
                <th>Solution du vendeur</th>
                <th>Seller Note</th>
                <th>Solution Final du Litige</th>
                <th>Litige Accepted</th>
                <th>Feedback Id</th>
                <th>raison de refus du Litige</th>
                <th>Soluttion</th>
                <th>Est Terminé</th>
                <th>En attende d'acceptation</th>
                <th colspan="3">Action</th>

            </tr>
        </thead>
        <tbody>
        @foreach($orderLitiges as $orderLitige)
            <tr>
                <td>{{ $orderLitige->order_id }}</td>
                <td>{{ $orderLitige->customer ? $orderLitige->customer->name: "Donnée manquante ..."}}</td>
                <td>{{ $orderLitige->seller ? $orderLitige->seller->name: "Donnée manquante ..." }}</td>
                <td>{{ \Illuminate\Support\Str::limit($orderLitige->customer_solution, 30, '....') }}</td>
                <td>{{ \Illuminate\Support\Str::limit($orderLitige->seller_solution, 30, '....') }}</td>
                <td>{{ \Illuminate\Support\Str::limit($orderLitige->seller_note, 30, '....') }}</td>
                <td>{{ \Illuminate\Support\Str::limit($orderLitige->final_litige_solution, 30, '....') }}</td>
                <td>
                    @if($orderLitige->litige_accepted == true)
                        <button class="btn btn-success" disabled>
                            Accepté
                        </button>
                    @else
                        <button class="btn btn-secondary" disabled>
                            Non Accépté
                        </button>
                    @endif
                </td>
                <td>{{ $orderLitige->feedback_id }}</td>
                <td>{{ $orderLitige->litige_rejected_reason }}</td>
                <td>{{ $orderLitige->solved }}</td>

                <td>
                    @if($orderLitige->is_completed == true)
                        <button class="btn btn-success" disabled>
                            Approuver
                        </button>
                    @else
                        <button class="btn btn-secondary" disabled>
                            Non Approuver
                        </button>
                    @endif
                </td>

                <td>{{ $orderLitige->is_waiting_admin_acceptation == true ? 'Oui' : 'Non' }}</td>
                <td class=" text-center">
                    {!! Form::open(['route' => ['orderLitiges.destroy', $orderLitige->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('orderLitiges.solved', [$orderLitige->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-check"></i></a>
                        <a href="{!! route('orderLitiges.approved', [$orderLitige->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-certificate"></i></a>
                        <a href="{!! route('orderLitiges.rejected', [$orderLitige->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-times"></i></a>
                        <a href="{!! route('orderLitiges.show', [$orderLitige->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                        <a href="{!! route('orderLitiges.edit', [$orderLitige->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("Are you sure want to delete this record ?")']) !!}
                    </div>
                    {!! Form::close() !!}
                </td>

            </tr>
        @endforeach
        </tbody>
    </table>
</div>
