<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    {!! Form::number('customer_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Seller Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('seller_id', 'Seller Id:') !!}
    {!! Form::number('seller_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Seller Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('seller_type', 'Seller Type:') !!}
    {!! Form::text('seller_type', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Is Waiting Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_waiting', 'Is Waiting:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_waiting', 0) !!}
        {!! Form::checkbox('is_waiting', '1', null) !!}
    </label>
</div>


<!-- Current Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('current_status', 'Current Status:') !!}
    {!! Form::text('current_status', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Rating Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rating', 'Rating:') !!}
    {!! Form::text('rating', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Notes Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('notes', 'Notes:') !!}
    {!! Form::textarea('notes', null, ['class' => 'form-control']) !!}
</div>

<!-- Note Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('note_description', 'Note Description:') !!}
    {!! Form::textarea('note_description', null, ['class' => 'form-control']) !!}
</div>

<!-- Note Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('note_date', 'Note Date:') !!}
    {!! Form::text('note_date', null, ['class' => 'form-control','id'=>'note_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#note_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Reports Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('reports', 'Reports:') !!}
    {!! Form::textarea('reports', null, ['class' => 'form-control']) !!}
</div>

<!-- Report Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('report_description', 'Report Description:') !!}
    {!! Form::textarea('report_description', null, ['class' => 'form-control']) !!}
</div>

<!-- Report Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('report_date', 'Report Date:') !!}
    {!! Form::text('report_date', null, ['class' => 'form-control','id'=>'report_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#report_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Payment Method Slug Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_method_slug', 'Payment Method Slug:') !!}
    {!! Form::text('payment_method_slug', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Delivery Formula Slug Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_formula_slug', 'Delivery Formula Slug:') !!}
    {!! Form::text('delivery_formula_slug', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Delivery Fees Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_fees', 'Delivery Fees:') !!}
    {!! Form::text('delivery_fees', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('amount', 'Amount:') !!}
    {!! Form::text('amount', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Currency Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('currency_code', 'Currency Code:') !!}
    {!! Form::text('currency_code', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Is Delivered Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_delivered', 'Is Delivered:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_delivered', 0) !!}
        {!! Form::checkbox('is_delivered', '1', null) !!}
    </label>
</div>


<!-- Is Waiting Payment Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_waiting_payment', 'Is Waiting Payment:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_waiting_payment', 0) !!}
        {!! Form::checkbox('is_waiting_payment', '1', null) !!}
    </label>
</div>


<!-- Is Waiting Note Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_waiting_note', 'Is Waiting Note:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_waiting_note', 0) !!}
        {!! Form::checkbox('is_waiting_note', '1', null) !!}
    </label>
</div>


<!-- Delivery Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_date', 'Delivery Date:') !!}
    {!! Form::text('delivery_date', null, ['class' => 'form-control','id'=>'delivery_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#delivery_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Task Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('task_id', 'Task Id:') !!}
    {!! Form::text('task_id', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Merchant Assignments Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('merchant_assignments', 'Merchant Assignments:') !!}
    {!! Form::textarea('merchant_assignments', null, ['class' => 'form-control']) !!}
</div>

<!-- Merchant Assignment Accepted Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('merchant_assignment_accepted', 'Merchant Assignment Accepted:') !!}
    {!! Form::textarea('merchant_assignment_accepted', null, ['class' => 'form-control']) !!}
</div>

<!-- Reference Field -->
<div class="form-group col-sm-6">
    {!! Form::label('reference', 'Reference:') !!}
    {!! Form::text('reference', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Seller Has Delivred Field -->
<div class="form-group col-sm-6">
    {!! Form::label('seller_has_delivred', 'Seller Has Delivred:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('seller_has_delivred', 0) !!}
        {!! Form::checkbox('seller_has_delivred', '1', null) !!}
    </label>
</div>


<!-- Seller Has Confirmed Field -->
<div class="form-group col-sm-6">
    {!! Form::label('seller_has_confirmed', 'Seller Has Confirmed:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('seller_has_confirmed', 0) !!}
        {!! Form::checkbox('seller_has_confirmed', '1', null) !!}
    </label>
</div>


<!-- Customer Has Confirmed Delivery Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_has_confirmed_delivery', 'Customer Has Confirmed Delivery:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('customer_has_confirmed_delivery', 0) !!}
        {!! Form::checkbox('customer_has_confirmed_delivery', '1', null) !!}
    </label>
</div>


<!-- Customer Delivery Signature Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_delivery_signature', 'Customer Delivery Signature:') !!}
    {!! Form::text('customer_delivery_signature', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Customer Delivery Signature At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_delivery_signature_at', 'Customer Delivery Signature At:') !!}
    {!! Form::text('customer_delivery_signature_at', null, ['class' => 'form-control','id'=>'customer_delivery_signature_at']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#customer_delivery_signature_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Is Customer Reported Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_customer_reported', 'Is Customer Reported:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_customer_reported', 0) !!}
        {!! Form::checkbox('is_customer_reported', '1', null) !!}
    </label>
</div>


<!-- Is Completed Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_completed', 'Is Completed:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_completed', 0) !!}
        {!! Form::checkbox('is_completed', '1', null) !!}
    </label>
</div>


<!-- Is Seller Paid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_seller_paid', 'Is Seller Paid:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_seller_paid', 0) !!}
        {!! Form::checkbox('is_seller_paid', '1', null) !!}
    </label>
</div>


<!-- Has Litige Field -->
<div class="form-group col-sm-6">
    {!! Form::label('has_litige', 'Has Litige:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('has_litige', 0) !!}
        {!! Form::checkbox('has_litige', '1', null) !!}
    </label>
</div>


<!-- Is Refunded Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_refunded', 'Is Refunded:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_refunded', 0) !!}
        {!! Form::checkbox('is_refunded', '1', null) !!}
    </label>
</div>


<!-- Is Litige Solved Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_litige_solved', 'Is Litige Solved:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_litige_solved', 0) !!}
        {!! Form::checkbox('is_litige_solved', '1', null) !!}
    </label>
</div>


<!-- Litige Solution Field -->
<div class="form-group col-sm-6">
    {!! Form::label('litige_solution', 'Litige Solution:') !!}
    {!! Form::text('litige_solution', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Is Returned Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_returned', 'Is Returned:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_returned', 0) !!}
        {!! Form::checkbox('is_returned', '1', null) !!}
    </label>
</div>


<!-- Is Customer Satisfied Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_customer_satisfied', 'Is Customer Satisfied:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_customer_satisfied', 0) !!}
        {!! Form::checkbox('is_customer_satisfied', '1', null) !!}
    </label>
</div>


<!-- Warranty Period Days Field -->
<div class="form-group col-sm-6">
    {!! Form::label('warranty_period_days', 'Warranty Period Days:') !!}
    {!! Form::number('warranty_period_days', null, ['class' => 'form-control']) !!}
</div>

<!-- Warranty Period Last Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('warranty_period_last_date', 'Warranty Period Last Date:') !!}
    {!! Form::text('warranty_period_last_date', null, ['class' => 'form-control','id'=>'warranty_period_last_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#warranty_period_last_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Return Delivery Signature Field -->
<div class="form-group col-sm-6">
    {!! Form::label('return_delivery_signature', 'Return Delivery Signature:') !!}
    {!! Form::text('return_delivery_signature', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Return Delivery Signature At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('return_delivery_signature_at', 'Return Delivery Signature At:') !!}
    {!! Form::text('return_delivery_signature_at', null, ['class' => 'form-control','id'=>'return_delivery_signature_at']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#return_delivery_signature_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('orders.index') }}" class="btn btn-light">Cancel</a>
</div>
