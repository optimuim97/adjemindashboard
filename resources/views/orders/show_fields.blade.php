<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    <p>{{ $order->customer_id }}</p>
</div>

<!-- Seller Id Field -->
<div class="form-group">
    {!! Form::label('seller_id', 'Seller Id:') !!}
    <p>{{ $order->seller_id }}</p>
</div>

<!-- Seller Type Field -->
<div class="form-group">
    {!! Form::label('seller_type', 'Seller Type:') !!}
    <p>{{ $order->seller_type }}</p>
</div>

<!-- Is Waiting Field -->
<div class="form-group">
    {!! Form::label('is_waiting', 'Is Waiting:') !!}
    <p>{{ $order->is_waiting }}</p>
</div>

<!-- Current Status Field -->
<div class="form-group">
    {!! Form::label('current_status', 'Current Status:') !!}
    <p>{{ $order->current_status }}</p>
</div>

<!-- Rating Field -->
<div class="form-group">
    {!! Form::label('rating', 'Rating:') !!}
    <p>{{ $order->rating }}</p>
</div>

<!-- Notes Field -->
<div class="form-group">
    {!! Form::label('notes', 'Notes:') !!}
    <p>{{ $order->notes }}</p>
</div>

<!-- Note Description Field -->
<div class="form-group">
    {!! Form::label('note_description', 'Note Description:') !!}
    <p>{{ $order->note_description }}</p>
</div>

<!-- Note Date Field -->
<div class="form-group">
    {!! Form::label('note_date', 'Note Date:') !!}
    <p>{{ $order->note_date }}</p>
</div>

<!-- Reports Field -->
<div class="form-group">
    {!! Form::label('reports', 'Reports:') !!}
    <p>{{ $order->reports }}</p>
</div>

<!-- Report Description Field -->
<div class="form-group">
    {!! Form::label('report_description', 'Report Description:') !!}
    <p>{{ $order->report_description }}</p>
</div>

<!-- Report Date Field -->
<div class="form-group">
    {!! Form::label('report_date', 'Report Date:') !!}
    <p>{{ $order->report_date }}</p>
</div>

<!-- Payment Method Slug Field -->
<div class="form-group">
    {!! Form::label('payment_method_slug', 'Payment Method Slug:') !!}
    <p>{{ $order->payment_method_slug }}</p>
</div>

<!-- Delivery Formula Slug Field -->
<div class="form-group">
    {!! Form::label('delivery_formula_slug', 'Delivery Formula Slug:') !!}
    <p>{{ $order->delivery_formula_slug }}</p>
</div>

<!-- Delivery Fees Field -->
<div class="form-group">
    {!! Form::label('delivery_fees', 'Delivery Fees:') !!}
    <p>{{ $order->delivery_fees }}</p>
</div>

<!-- Amount Field -->
<div class="form-group">
    {!! Form::label('amount', 'Amount:') !!}
    <p>{{ $order->amount }}</p>
</div>

<!-- Currency Code Field -->
<div class="form-group">
    {!! Form::label('currency_code', 'Currency Code:') !!}
    <p>{{ $order->currency_code }}</p>
</div>

<!-- Is Delivered Field -->
<div class="form-group">
    {!! Form::label('is_delivered', 'Is Delivered:') !!}
    <p>{{ $order->is_delivered }}</p>
</div>

<!-- Is Waiting Payment Field -->
<div class="form-group">
    {!! Form::label('is_waiting_payment', 'Is Waiting Payment:') !!}
    <p>{{ $order->is_waiting_payment }}</p>
</div>

<!-- Is Waiting Note Field -->
<div class="form-group">
    {!! Form::label('is_waiting_note', 'Is Waiting Note:') !!}
    <p>{{ $order->is_waiting_note }}</p>
</div>

<!-- Delivery Date Field -->
<div class="form-group">
    {!! Form::label('delivery_date', 'Delivery Date:') !!}
    <p>{{ $order->delivery_date }}</p>
</div>

<!-- Task Id Field -->
<div class="form-group">
    {!! Form::label('task_id', 'Task Id:') !!}
    <p>{{ $order->task_id }}</p>
</div>

<!-- Merchant Assignments Field -->
<div class="form-group">
    {!! Form::label('merchant_assignments', 'Merchant Assignments:') !!}
    <p>{{ $order->merchant_assignments }}</p>
</div>

<!-- Merchant Assignment Accepted Field -->
<div class="form-group">
    {!! Form::label('merchant_assignment_accepted', 'Merchant Assignment Accepted:') !!}
    <p>{{ $order->merchant_assignment_accepted }}</p>
</div>

<!-- Reference Field -->
<div class="form-group">
    {!! Form::label('reference', 'Reference:') !!}
    <p>{{ $order->reference }}</p>
</div>

<!-- Seller Has Delivred Field -->
<div class="form-group">
    {!! Form::label('seller_has_delivred', 'Seller Has Delivred:') !!}
    <p>{{ $order->seller_has_delivred }}</p>
</div>

<!-- Seller Has Confirmed Field -->
<div class="form-group">
    {!! Form::label('seller_has_confirmed', 'Seller Has Confirmed:') !!}
    <p>{{ $order->seller_has_confirmed }}</p>
</div>

<!-- Customer Has Confirmed Delivery Field -->
<div class="form-group">
    {!! Form::label('customer_has_confirmed_delivery', 'Customer Has Confirmed Delivery:') !!}
    <p>{{ $order->customer_has_confirmed_delivery }}</p>
</div>

<!-- Customer Delivery Signature Field -->
<div class="form-group">
    {!! Form::label('customer_delivery_signature', 'Customer Delivery Signature:') !!}
    <p>{{ $order->customer_delivery_signature }}</p>
</div>

<!-- Customer Delivery Signature At Field -->
<div class="form-group">
    {!! Form::label('customer_delivery_signature_at', 'Customer Delivery Signature At:') !!}
    <p>{{ $order->customer_delivery_signature_at }}</p>
</div>

<!-- Is Customer Reported Field -->
<div class="form-group">
    {!! Form::label('is_customer_reported', 'Is Customer Reported:') !!}
    <p>{{ $order->is_customer_reported }}</p>
</div>

<!-- Is Completed Field -->
<div class="form-group">
    {!! Form::label('is_completed', 'Is Completed:') !!}
    <p>{{ $order->is_completed }}</p>
</div>

<!-- Is Seller Paid Field -->
<div class="form-group">
    {!! Form::label('is_seller_paid', 'Is Seller Paid:') !!}
    <p>{{ $order->is_seller_paid }}</p>
</div>

<!-- Has Litige Field -->
<div class="form-group">
    {!! Form::label('has_litige', 'Has Litige:') !!}
    <p>{{ $order->has_litige }}</p>
</div>

<!-- Is Refunded Field -->
<div class="form-group">
    {!! Form::label('is_refunded', 'Is Refunded:') !!}
    <p>{{ $order->is_refunded }}</p>
</div>

<!-- Is Litige Solved Field -->
<div class="form-group">
    {!! Form::label('is_litige_solved', 'Is Litige Solved:') !!}
    <p>{{ $order->is_litige_solved }}</p>
</div>

<!-- Litige Solution Field -->
<div class="form-group">
    {!! Form::label('litige_solution', 'Litige Solution:') !!}
    <p>{{ $order->litige_solution }}</p>
</div>

<!-- Is Returned Field -->
<div class="form-group">
    {!! Form::label('is_returned', 'Is Returned:') !!}
    <p>{{ $order->is_returned }}</p>
</div>

<!-- Is Customer Satisfied Field -->
<div class="form-group">
    {!! Form::label('is_customer_satisfied', 'Is Customer Satisfied:') !!}
    <p>{{ $order->is_customer_satisfied }}</p>
</div>

<!-- Warranty Period Days Field -->
<div class="form-group">
    {!! Form::label('warranty_period_days', 'Warranty Period Days:') !!}
    <p>{{ $order->warranty_period_days }}</p>
</div>

<!-- Warranty Period Last Date Field -->
<div class="form-group">
    {!! Form::label('warranty_period_last_date', 'Warranty Period Last Date:') !!}
    <p>{{ $order->warranty_period_last_date }}</p>
</div>

<!-- Return Delivery Signature Field -->
<div class="form-group">
    {!! Form::label('return_delivery_signature', 'Return Delivery Signature:') !!}
    <p>{{ $order->return_delivery_signature }}</p>
</div>

<!-- Return Delivery Signature At Field -->
<div class="form-group">
    {!! Form::label('return_delivery_signature_at', 'Return Delivery Signature At:') !!}
    <p>{{ $order->return_delivery_signature_at }}</p>
</div>

