<table class="table table-responsive-sm table-striped table-bordered" id="ordersTable">
    <thead>
    <tr>
        <th scope="col">Action</th>
        <th scope="col">customer_id</th><th scope="col">seller_id</th><th scope="col">seller_type</th><th scope="col">is_waiting</th><th scope="col">current_status</th><th scope="col">rating</th><th scope="col">notes</th><th scope="col">note_description</th><th scope="col">note_date</th><th scope="col">reports</th><th scope="col">report_description</th><th scope="col">report_date</th><th scope="col">payment_method_slug</th><th scope="col">delivery_formula_slug</th><th scope="col">delivery_fees</th><th scope="col">amount</th><th scope="col">currency_code</th><th scope="col">is_delivered</th><th scope="col">is_waiting_payment</th><th scope="col">is_waiting_note</th><th scope="col">delivery_date</th><th scope="col">task_id</th><th scope="col">merchant_assignments</th><th scope="col">merchant_assignment_accepted</th><th scope="col">reference</th><th scope="col">seller_has_delivred</th><th scope="col">seller_has_confirmed</th><th scope="col">customer_has_confirmed_delivery</th><th scope="col">customer_delivery_signature</th><th scope="col">customer_delivery_signature_at</th><th scope="col">is_customer_reported</th><th scope="col">is_completed</th><th scope="col">is_seller_paid</th><th scope="col">has_litige</th><th scope="col">is_refunded</th><th scope="col">is_litige_solved</th><th scope="col">litige_solution</th><th scope="col">is_returned</th><th scope="col">is_customer_satisfied</th><th scope="col">warranty_period_days</th><th scope="col">warranty_period_last_date</th><th scope="col">return_delivery_signature</th><th scope="col">return_delivery_signature_at</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
