<!-- Nom Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nom', 'Nom:') !!}
    {!! Form::text('nom', null, ['class' => 'form-control','maxlength' => 191,'maxlength' => 191,'maxlength' => 191]) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control','maxlength' => 100,'maxlength' => 100,'maxlength' => 100]) !!}
</div>

<!-- Identifiant Field -->
<div class="form-group col-sm-6">
    {!! Form::label('identifiant', 'Identifiant:') !!}
    {!! Form::text('identifiant', null, ['class' => 'form-control','maxlength' => 100,'maxlength' => 100,'maxlength' => 100]) !!}
</div>

<!-- Rccm Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rccm', 'Rccm:') !!}
    {!! Form::text('rccm', null, ['class' => 'form-control','maxlength' => 30,'maxlength' => 30,'maxlength' => 30]) !!}
</div>

<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    {!! Form::number('customer_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Photo Profil Field -->
<div class="form-group col-sm-6">
    {!! Form::label('photo_profil', 'Photo Profil:') !!}
    {!! Form::text('photo_profil', null, ['class' => 'form-control','maxlength' => 191,'maxlength' => 191,'maxlength' => 191]) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('boutiques.index') }}" class="btn btn-light">Cancel</a>
</div>
