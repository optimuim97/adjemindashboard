<!-- Nom Field -->
<div class="form-group">
    {!! Form::label('nom', 'Nom:') !!}
    <p>{{ $boutique->nom }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $boutique->email }}</p>
</div>

<!-- Identifiant Field -->
<div class="form-group">
    {!! Form::label('identifiant', 'Identifiant:') !!}
    <p>{{ $boutique->identifiant }}</p>
</div>

<!-- Rccm Field -->
<div class="form-group">
    {!! Form::label('rccm', 'Rccm:') !!}
    <p>{{ $boutique->rccm }}</p>
</div>

<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    <p>{{ $boutique->customer_id }}</p>
</div>

<!-- Photo Profil Field -->
<div class="form-group">
    {!! Form::label('photo_profil', 'Photo Profil:') !!}
    <p>{{ $boutique->photo_profil }}</p>
</div>

