'use strict';

let tableName = '#customersTable';
$(tableName).DataTable({
    scrollX: true,
    deferRender: true,
    scroller: true,
    processing: true,
    serverSide: true,
    'order': [[0, 'asc']],
    ajax: {
        url: recordsURL,
    },
    columnDefs: [
        {
            'targets': [49],
            'orderable': false,
            'className': 'text-center',
            'width': '8%',
        },
    ],
    columns: [
        {
            data: 'first_name',
            name: 'first_name'
        },{
            data: 'last_name',
            name: 'last_name'
        },{
            data: 'name',
            name: 'name'
        },{
            data: 'phone_number',
            name: 'phone_number'
        },{
            data: 'phone',
            name: 'phone'
        },{
            data: 'is_phone_verified',
            name: 'is_phone_verified'
        },{
            data: 'phone_verified_at',
            name: 'phone_verified_at'
        },{
            data: 'is_active',
            name: 'is_active'
        },{
            data: 'activation_date',
            name: 'activation_date'
        },{
            data: 'dial_code',
            name: 'dial_code'
        },{
            data: 'country_code',
            name: 'country_code'
        },{
            data: 'email',
            name: 'email'
        },{
            data: 'email_verified_at',
            name: 'email_verified_at'
        },{
            data: 'photo_id',
            name: 'photo_id'
        },{
            data: 'photo_url', render: function (data, default_image="https://imgur.com/a/PK9fTF1" ) {
                return `<img src='${data != null ? data : default_image}'></img>`;
            },
            name: 'photo_url'
        },{
            data: 'photo_mime',
            name: 'photo_mime'
        },{
            data: 'photo_height',
            name: 'photo_height'
        },{
            data: 'photo_width',
            name: 'photo_width'
        },{
            data: 'photo_length',
            name: 'photo_length'
        },{
            data: 'gender',
            name: 'gender'
        },{
            data: 'bio',
            name: 'bio'
        },{
            data: 'birthday',
            name: 'birthday'
        },{
            data: 'birth_location',
            name: 'birth_location'
        },{
            data: 'language',
            name: 'language'
        },{
            data: 'facebook_id',
            name: 'facebook_id'
        },{
            data: 'twitter_id',
            name: 'twitter_id'
        },{
            data: 'google_id',
            name: 'google_id'
        },{
            data: 'is_pro_seller',
            name: 'is_pro_seller'
        },{
            data: 'has_adjemin_pay',
            name: 'has_adjemin_pay'
        },{
            data: 'origin',
            name: 'origin'
        },{
            data: 'password',
            name: 'password'
        },{
            data: 'wallet_password',
            name: 'wallet_password'
        },{
            data: 'is_wallet_active',
            name: 'is_wallet_active'
        },{
            data: 'wallet_activation_date',
            name: 'wallet_activation_date'
        },{
            data: 'default_currency',
            name: 'default_currency'
        },{
            data: 'identity_card_ref',
            name: 'identity_card_ref'
        },{
            data: 'identity_card_link_recto',
            name: 'identity_card_link_recto'
        },{
            data: 'identity_card_link_verso',
            name: 'identity_card_link_verso'
        },{
            data: 'identity_card_type',
            name: 'identity_card_type'
        },{
            data: 'account_type',
            name: 'account_type'
        },{
            data: 'company_name',
            name: 'company_name'
        },{
            data: 'company_country',
            name: 'company_country'
        },{
            data: 'company_reference',
            name: 'company_reference'
        },{
            data: 'last_location_gps',
            name: 'last_location_gps'
        },{
            data: 'rating',
            name: 'rating'
        },{
            data: 'username',
            name: 'username'
        },{
            data: 'godfather_username',
            name: 'godfather_username'
        },{
            data: 'has_badge',
            name: 'has_badge'
        },{
            data: 'is_email_registration',
            name: 'is_email_registration'
        },
        {
            data: function (row) {
                let url = recordsURL + row.id;
                let data = [
                {
                    'id': row.id,
                    'url': url + '/edit',
                }];

                return prepareTemplateRender('#customersTemplate',
                    data);
            }, name: 'id',
        },
    ],
});

$(document).on('click', '.delete-btn', function (event) {
    let recordId = $(event.currentTarget).data('id');
    deleteItem(recordsURL + recordId, tableName, 'Customer');
});
