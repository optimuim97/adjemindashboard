'use strict';

let tableName = '#productsTable';
$(tableName).DataTable({
    scrollX: true,
    deferRender: true,
    scroller: true,
    processing: true,
    serverSide: true,
    'order': [[0, 'asc']],
    ajax: {
        url: recordsURL,
    },
    columnDefs: [
        {
            'targets': [56],
            'orderable': false,
            'className': 'text-center',
            'width': '8%',
        },
    ],
    columns: [
        {
            data: 'title',
            name: 'title'
        },{
            data: 'description',
            name: 'description'
        },{
            data: 'description_metadata',
            name: 'description_metadata'
        },{
            data: 'price',
            name: 'price'
        },{
            data: 'original_price',
            name: 'original_price'
        },{
            data: 'fees',
            name: 'fees'
        },{
            data: 'currency_slug',
            name: 'currency_slug'
        },{
            data: 'old_price',
            name: 'old_price'
        },{
            data: 'has_promo',
            name: 'has_promo'
        },{
            data: 'promo_percentage',
            name: 'promo_percentage'
        },{
            data: 'medias',
            name: 'medias'
        },{
            data: 'cover_id',
            name: 'cover_id'
        },{
            data: 'cover_type',
            name: 'cover_type'
        },{
            data: 'cover_url',
            name: 'cover_url'
        },{
            data: 'cover_thumbnail',
            name: 'cover_thumbnail'
        },{
            data: 'cover_duration',
            name: 'cover_duration'
        },{
            data: 'cover_width',
            name: 'cover_width'
        },{
            data: 'cover_height',
            name: 'cover_height'
        },{
            data: 'cover_blurhash',
            name: 'cover_blurhash'
        },{
            data: 'location_name',
            name: 'location_name'
        },{
            data: 'location_address',
            name: 'location_address'
        },{
            data: 'location_lat',
            name: 'location_lat'
        },{
            data: 'location_lng',
            name: 'location_lng'
        },{
            data: 'category_id',
            name: 'category_id'
        },{
            data: 'category_meta',
            name: 'category_meta'
        },{
            data: 'customer_id',
            name: 'customer_id'
        },{
            data: 'is_from_shop',
            name: 'is_from_shop'
        },{
            data: 'is_sold',
            name: 'is_sold'
        },{
            data: 'sold_at',
            name: 'sold_at'
        },{
            data: 'initiale_count',
            name: 'initiale_count'
        },{
            data: 'is_firm_price',
            name: 'is_firm_price'
        },{
            data: 'condition_id',
            name: 'condition_id'
        },{
            data: 'published_at',
            name: 'published_at'
        },{
            data: 'is_published',
            name: 'is_published'
        },{
            data: 'deleted_by',
            name: 'deleted_by'
        },{
            data: 'deleted_creator',
            name: 'deleted_creator'
        },{
            data: 'sold_count',
            name: 'sold_count'
        },{
            data: 'delivery_services',
            name: 'delivery_services'
        },{
            data: 'slug',
            name: 'slug'
        },{
            data: 'link',
            name: 'link'
        },{
            data: 'has_delivery',
            name: 'has_delivery'
        },{
            data: 'has_ordering',
            name: 'has_ordering'
        },{
            data: 'order_payment_methods',
            name: 'order_payment_methods'
        },{
            data: 'visibility_deadline',
            name: 'visibility_deadline'
        },{
            data: 'tags',
            name: 'tags'
        },{
            data: 'is_garentie',
            name: 'is_garentie'
        },{
            data: 'duree_garentie',
            name: 'duree_garentie'
        },{
            data: 'is_gain_shop',
            name: 'is_gain_shop'
        },{
            data: 'is_exclusive_price',
            name: 'is_exclusive_price'
        },{
            data: 'is_promo_gain_shop',
            name: 'is_promo_gain_shop'
        },{
            data: 'is_selling',
            name: 'is_selling'
        },{
            data: 'promo_gain_shop',
            name: 'promo_gain_shop'
        },{
            data: 'promo_gain_shop_price',
            name: 'promo_gain_shop_price'
        },{
            data: 'is_night_market',
            name: 'is_night_market'
        },{
            data: 'promo_night_market_price',
            name: 'promo_night_market_price'
        },{
            data: 'promo_night_market_percentage',
            name: 'promo_night_market_percentage'
        },
        {
            data: function (row) {
                let url = recordsURL + row.id;
                let data = [
                {
                    'id': row.id,
                    'url': url + '/edit',
                }];

                return prepareTemplateRender('#productsTemplate',
                    data);
            }, name: 'id',
        },
    ],
});

$(document).on('click', 'delete-btn', function (event) {
    let recordId = $(event.cuerrentTarget).data('id');
    deleteItem(recordsURL + recordId, tableName, 'Product');
});
