'use strict';

let tableName = '#orderLitigesTable';
$(tableName).DataTable({
    scrollX: true,
    deferRender: true,
    scroller: true,
    processing: true,
    serverSide: true,
    'order': [[0, 'asc']],
    ajax: {
        url: recordsURL,
    },
    columnDefs: [
        {
            'targets': [13],
            'orderable': false,
            'className': 'text-center',
            'width': '8%',
        },
    ],
    columns: [
        {
            data: 'order_id',
            name: 'order_id'
        },{
            data: 'customer_id',
            name: 'customer_id'
        },{
            data: 'seller_id',
            name: 'seller_id'
        },{
            data: 'customer_solution',
            name: 'customer_solution'
        },{
            data: 'seller_solution',
            name: 'seller_solution'
        },{
            data: 'seller_note',
            name: 'seller_note'
        },{
            data: 'final_litige_solution',
            name: 'final_litige_solution'
        },{
            data: 'litige_accepted',
            name: 'litige_accepted'
        },{
            data: 'feedback_id',
            name: 'feedback_id'
        },{
            data: 'litige_rejected_reason',
            name: 'litige_rejected_reason'
        },{
            data: 'solved',
            name: 'solved'
        },{
            data: 'is_completed',
            name: 'is_completed'
        },{
            data: 'is_waiting_admin_acceptation',
            name: 'is_waiting_admin_acceptation'
        },
        {
            data: function (row) {
                let url = recordsURL + row.id;
                let data = [
                {
                    'id': row.id,
                    'url': url + '/edit',
                }];
                                
                return prepareTemplateRender('#orderLitigesTemplate',
                    data);
            }, name: 'id',
        },
    ],
});

$(document).on('click', '.delete-btn', function (event) {
    let recordId = $(event.currentTarget).data('id');
    deleteItem(recordsURL + recordId, tableName, 'Order Litige');
});
