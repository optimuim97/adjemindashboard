<?php

namespace App\Repositories;

use App\Models\Nescafe;
use App\Repositories\BaseRepository;

/**
 * Class NescafeRepository
 * @package App\Repositories
 * @version August 16, 2021, 4:40 pm UTC
*/

class NescafeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Nescafe::class;
    }
}
