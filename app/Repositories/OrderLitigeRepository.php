<?php

namespace App\Repositories;

use App\Models\OrderLitige;
use App\Repositories\BaseRepository;

/**
 * Class OrderLitigeRepository
 * @package App\Repositories
 * @version August 18, 2021, 4:47 pm UTC
*/

class OrderLitigeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'order_id',
        'customer_id',
        'seller_id',
        'customer_solution',
        'seller_solution',
        'seller_note',
        'final_litige_solution',
        'litige_accepted',
        'feedback_id',
        'litige_rejected_reason',
        'solved',
        'is_completed',
        'is_waiting_admin_acceptation'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrderLitige::class;
    }
}
