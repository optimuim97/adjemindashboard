<?php

namespace App\Repositories;

use App\Models\Customer;
use App\Repositories\BaseRepository;

/**
 * Class CustomerRepository
 * @package App\Repositories
 * @version August 16, 2021, 5:38 pm UTC
*/

class CustomerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'first_name',
        'last_name',
        'name',
        'phone_number',
        'phone',
        'is_phone_verified',
        'phone_verified_at',
        'is_active',
        'activation_date',
        'dial_code',
        'country_code',
        'email',
        'email_verified_at',
        'photo_id',
        'photo_url',
        'photo_mime',
        'photo_height',
        'photo_width',
        'photo_length',
        'gender',
        'bio',
        'birthday',
        'birth_location',
        'language',
        'facebook_id',
        'twitter_id',
        'google_id',
        'is_pro_seller',
        'has_adjemin_pay',
        'origin',
        'password',
        'wallet_password',
        'is_wallet_active',
        'wallet_activation_date',
        'default_currency',
        'identity_card_ref',
        'identity_card_link_recto',
        'identity_card_link_verso',
        'identity_card_type',
        'account_type',
        'company_name',
        'company_country',
        'company_reference',
        'last_location_gps',
        'rating',
        'username',
        'godfather_username',
        'has_badge',
        'is_email_registration'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Customer::class;
    }
}
