<?php

namespace App\Repositories;

use App\Models\Order;
use App\Repositories\BaseRepository;

/**
 * Class OrderRepository
 * @package App\Repositories
 * @version August 16, 2021, 6:09 pm UTC
*/

class OrderRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer_id',
        'seller_id',
        'seller_type',
        'is_waiting',
        'current_status',
        'rating',
        'notes',
        'note_description',
        'note_date',
        'reports',
        'report_description',
        'report_date',
        'payment_method_slug',
        'delivery_formula_slug',
        'delivery_fees',
        'amount',
        'currency_code',
        'is_delivered',
        'is_waiting_payment',
        'is_waiting_note',
        'delivery_date',
        'task_id',
        'merchant_assignments',
        'merchant_assignment_accepted',
        'reference',
        'seller_has_delivred',
        'seller_has_confirmed',
        'customer_has_confirmed_delivery',
        'customer_delivery_signature',
        'customer_delivery_signature_at',
        'is_customer_reported',
        'is_completed',
        'is_seller_paid',
        'has_litige',
        'is_refunded',
        'is_litige_solved',
        'litige_solution',
        'is_returned',
        'is_customer_satisfied',
        'warranty_period_days',
        'warranty_period_last_date',
        'return_delivery_signature',
        'return_delivery_signature_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Order::class;
    }
}
