<?php

namespace App\Repositories;

use App\Models\Camion;
use App\Repositories\BaseRepository;

/**
 * Class CamionRepository
 * @package App\Repositories
 * @version August 16, 2021, 4:38 pm UTC
*/

class CamionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'years'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Camion::class;
    }
}
