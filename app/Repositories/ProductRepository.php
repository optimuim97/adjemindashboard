<?php

namespace App\Repositories;

use App\Models\Product;
use App\Repositories\BaseRepository;

/**
 * Class ProductRepository
 * @package App\Repositories
 * @version August 16, 2021, 5:43 pm UTC
*/

class ProductRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'description',
        'description_metadata',
        'price',
        'original_price',
        'fees',
        'currency_slug',
        'old_price',
        'has_promo',
        'promo_percentage',
        'medias',
        'cover_id',
        'cover_type',
        'cover_url',
        'cover_thumbnail',
        'cover_duration',
        'cover_width',
        'cover_height',
        'cover_blurhash',
        'location_name',
        'location_address',
        'location_lat',
        'location_lng',
        'category_id',
        'category_meta',
        'customer_id',
        'is_from_shop',
        'is_sold',
        'sold_at',
        'initiale_count',
        'is_firm_price',
        'condition_id',
        'published_at',
        'is_published',
        'deleted_by',
        'deleted_creator',
        'sold_count',
        'delivery_services',
        'slug',
        'link',
        'has_delivery',
        'has_ordering',
        'order_payment_methods',
        'visibility_deadline',
        'tags',
        'is_garentie',
        'duree_garentie',
        'is_gain_shop',
        'is_exclusive_price',
        'is_promo_gain_shop',
        'is_selling',
        'promo_gain_shop',
        'promo_gain_shop_price',
        'is_night_market',
        'promo_night_market_price',
        'promo_night_market_percentage'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Product::class;
    }
}
