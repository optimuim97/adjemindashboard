<?php

namespace App\Repositories;

use App\Models\Boutique;
use App\Repositories\BaseRepository;

/**
 * Class BoutiqueRepository
 * @package App\Repositories
 * @version August 16, 2021, 6:32 pm UTC
*/

class BoutiqueRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nom',
        'email',
        'identifiant',
        'rccm',
        'customer_id',
        'photo_profil'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Boutique::class;
    }
}
