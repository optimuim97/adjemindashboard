<?php

namespace App\Http\Controllers;

use App\DataTables\BoutiqueDataTable;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\CreateBoutiqueRequest;
use App\Http\Requests\UpdateBoutiqueRequest;
use App\Repositories\BoutiqueRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Datatables;

class BoutiqueController extends AppBaseController
{
    /** @var  BoutiqueRepository */
    private $boutiqueRepository;

    public function __construct(BoutiqueRepository $boutiqueRepo)
    {
        $this->boutiqueRepository = $boutiqueRepo;
    }

    /**
     * Display a listing of the Boutique.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return Datatables::of((new BoutiqueDataTable())->get())->make(true);
        }
    
        return view('boutiques.index');
    }

    /**
     * Show the form for creating a new Boutique.
     *
     * @return Response
     */
    public function create()
    {
        return view('boutiques.create');
    }

    /**
     * Store a newly created Boutique in storage.
     *
     * @param CreateBoutiqueRequest $request
     *
     * @return Response
     */
    public function store(CreateBoutiqueRequest $request)
    {
        $input = $request->all();

        $boutique = $this->boutiqueRepository->create($input);

        Flash::success('Boutique saved successfully.');

        return redirect(route('boutiques.index'));
    }

    /**
     * Display the specified Boutique.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $boutique = $this->boutiqueRepository->find($id);

        if (empty($boutique)) {
            Flash::error('Boutique not found');

            return redirect(route('boutiques.index'));
        }

        return view('boutiques.show')->with('boutique', $boutique);
    }

    /**
     * Show the form for editing the specified Boutique.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $boutique = $this->boutiqueRepository->find($id);

        if (empty($boutique)) {
            Flash::error('Boutique not found');

            return redirect(route('boutiques.index'));
        }

        return view('boutiques.edit')->with('boutique', $boutique);
    }

    /**
     * Update the specified Boutique in storage.
     *
     * @param  int              $id
     * @param UpdateBoutiqueRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBoutiqueRequest $request)
    {
        $boutique = $this->boutiqueRepository->find($id);

        if (empty($boutique)) {
            Flash::error('Boutique not found');

            return redirect(route('boutiques.index'));
        }

        $boutique = $this->boutiqueRepository->update($request->all(), $id);

        Flash::success('Boutique updated successfully.');

        return redirect(route('boutiques.index'));
    }

    /**
     * Remove the specified Boutique from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $boutique = $this->boutiqueRepository->find($id);

        $boutique->delete();

        return $this->sendSuccess('Boutique deleted successfully.');
    }
}
