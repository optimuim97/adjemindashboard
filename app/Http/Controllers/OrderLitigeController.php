<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOrderLitigeRequest;
use App\Http\Requests\UpdateOrderLitigeRequest;
use App\Repositories\OrderLitigeRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\OrderLitige;
use Illuminate\Http\Request;
use Flash;
use Response;

class OrderLitigeController extends AppBaseController
{
    /** @var  OrderLitigeRepository */
    private $orderLitigeRepository;

    public function __construct(OrderLitigeRepository $orderLitigeRepo)
    {
        $this->orderLitigeRepository = $orderLitigeRepo;
    }

    /**
     * Display a listing of the OrderLitige.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $orderLitiges = $this->orderLitigeRepository->paginate(10);

        return view('order_litiges.index')
            ->with('orderLitiges', $orderLitiges);
    }

    /**
     * Show the form for creating a new OrderLitige.
     *
     * @return Response
     */
    public function create()
    {
        return view('order_litiges.create');
    }

    /**
     * Store a newly created OrderLitige in storage.
     *
     * @param CreateOrderLitigeRequest $request
     *
     * @return Response
     */
    public function store(CreateOrderLitigeRequest $request)
    {
        $input = $request->all();

        $orderLitige = $this->orderLitigeRepository->create($input);

        Flash::success('Order Litige saved successfully.');

        return redirect(route('orderLitiges.index'));
    }

    /**
     * Display the specified OrderLitige.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $orderLitige = $this->orderLitigeRepository->find($id);

        if (empty($orderLitige)) {
            Flash::error('Order Litige not found');

            return redirect(route('orderLitiges.index'));
        }

        return view('order_litiges.show')->with('orderLitige', $orderLitige);
    }

    /**
     * Show the form for editing the specified OrderLitige.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $orderLitige = $this->orderLitigeRepository->find($id);

        if (empty($orderLitige)) {
            Flash::error('Order Litige not found');

            return redirect(route('orderLitiges.index'));
        }

        return view('order_litiges.edit')->with('orderLitige', $orderLitige);
    }

    /**
     * Update the specified OrderLitige in storage.
     *
     * @param int $id
     * @param UpdateOrderLitigeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrderLitigeRequest $request)
    {
        $orderLitige = $this->orderLitigeRepository->find($id);

        if (empty($orderLitige)) {
            Flash::error('Order Litige not found');

            return redirect(route('orderLitiges.index'));
        }

        $orderLitige = $this->orderLitigeRepository->update($request->all(), $id);

        Flash::success('Order Litige updated successfully.');

        return redirect(route('orderLitiges.index'));
    }

    /**
     * Remove the specified OrderLitige from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $orderLitige = $this->orderLitigeRepository->find($id);

        if (empty($orderLitige)) {
            Flash::error('Order Litige not found');

            return redirect(route('orderLitiges.index'));
        }

        $this->orderLitigeRepository->delete($id);

        Flash::success('Order Litige deleted successfully.');

        return redirect(route('orderLitiges.index'));
    }


    public function rejected($id){
        $orderLitige = OrderLitige::findOrFail($id);
        $orderLitige->litige_accepted = false;
        $orderLitige->is_waiting_admin_acceptation = false;
        $orderLitige->save();
        toastr()->error('Rejeté !', 'Changement de status du litige');

        return back();
    }

    public function approved($id){
        $orderLitige = OrderLitige::findOrFail($id);
        $orderLitige->litige_accepted = true;
        $orderLitige->is_waiting_admin_acceptation = false;
        $orderLitige->save();
        toastr()->success('Approuvé', 'Changement de status du litige');

        return back();
    }

    public function solved($id){
        $orderLitige = OrderLitige::findOrFail($id);
        $orderLitige->solved = true;
        $orderLitige->save();
        toastr()->info('Résolue', 'Changement de status du litige');

        return back();
    }
}
