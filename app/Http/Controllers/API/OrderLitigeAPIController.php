<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateOrderLitigeAPIRequest;
use App\Http\Requests\API\UpdateOrderLitigeAPIRequest;
use App\Models\OrderLitige;
use App\Repositories\OrderLitigeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class OrderLitigeController
 * @package App\Http\Controllers\API
 */

class OrderLitigeAPIController extends AppBaseController
{
    /** @var  OrderLitigeRepository */
    private $orderLitigeRepository;

    public function __construct(OrderLitigeRepository $orderLitigeRepo)
    {
        $this->orderLitigeRepository = $orderLitigeRepo;
    }

    /**
     * Display a listing of the OrderLitige.
     * GET|HEAD /orderLitiges
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $orderLitiges = $this->orderLitigeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($orderLitiges->toArray(), 'Order Litiges retrieved successfully');
    }

    /**
     * Store a newly created OrderLitige in storage.
     * POST /orderLitiges
     *
     * @param CreateOrderLitigeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateOrderLitigeAPIRequest $request)
    {
        $input = $request->all();

        $orderLitige = $this->orderLitigeRepository->create($input);

        return $this->sendResponse($orderLitige->toArray(), 'Order Litige saved successfully');
    }

    /**
     * Display the specified OrderLitige.
     * GET|HEAD /orderLitiges/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var OrderLitige $orderLitige */
        $orderLitige = $this->orderLitigeRepository->find($id);

        if (empty($orderLitige)) {
            return $this->sendError('Order Litige not found');
        }

        return $this->sendResponse($orderLitige->toArray(), 'Order Litige retrieved successfully');
    }

    /**
     * Update the specified OrderLitige in storage.
     * PUT/PATCH /orderLitiges/{id}
     *
     * @param int $id
     * @param UpdateOrderLitigeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrderLitigeAPIRequest $request)
    {
        $input = $request->all();

        /** @var OrderLitige $orderLitige */
        $orderLitige = $this->orderLitigeRepository->find($id);

        if (empty($orderLitige)) {
            return $this->sendError('Order Litige not found');
        }

        $orderLitige = $this->orderLitigeRepository->update($input, $id);

        return $this->sendResponse($orderLitige->toArray(), 'OrderLitige updated successfully');
    }

    /**
     * Remove the specified OrderLitige from storage.
     * DELETE /orderLitiges/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var OrderLitige $orderLitige */
        $orderLitige = $this->orderLitigeRepository->find($id);

        if (empty($orderLitige)) {
            return $this->sendError('Order Litige not found');
        }

        $orderLitige->delete();

        return $this->sendSuccess('Order Litige deleted successfully');
    }
}
