<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBoutiqueAPIRequest;
use App\Http\Requests\API\UpdateBoutiqueAPIRequest;
use App\Models\Boutique;
use App\Repositories\BoutiqueRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class BoutiqueController
 * @package App\Http\Controllers\API
 */

class BoutiqueAPIController extends AppBaseController
{
    /** @var  BoutiqueRepository */
    private $boutiqueRepository;

    public function __construct(BoutiqueRepository $boutiqueRepo)
    {
        $this->boutiqueRepository = $boutiqueRepo;
    }

    /**
     * Display a listing of the Boutique.
     * GET|HEAD /boutiques
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $boutiques = $this->boutiqueRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($boutiques->toArray(), 'Boutiques retrieved successfully');
    }

    /**
     * Store a newly created Boutique in storage.
     * POST /boutiques
     *
     * @param CreateBoutiqueAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateBoutiqueAPIRequest $request)
    {
        $input = $request->all();

        $boutique = $this->boutiqueRepository->create($input);

        return $this->sendResponse($boutique->toArray(), 'Boutique saved successfully');
    }

    /**
     * Display the specified Boutique.
     * GET|HEAD /boutiques/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Boutique $boutique */
        $boutique = $this->boutiqueRepository->find($id);

        if (empty($boutique)) {
            return $this->sendError('Boutique not found');
        }

        return $this->sendResponse($boutique->toArray(), 'Boutique retrieved successfully');
    }

    /**
     * Update the specified Boutique in storage.
     * PUT/PATCH /boutiques/{id}
     *
     * @param int $id
     * @param UpdateBoutiqueAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBoutiqueAPIRequest $request)
    {
        $input = $request->all();

        /** @var Boutique $boutique */
        $boutique = $this->boutiqueRepository->find($id);

        if (empty($boutique)) {
            return $this->sendError('Boutique not found');
        }

        $boutique = $this->boutiqueRepository->update($input, $id);

        return $this->sendResponse($boutique->toArray(), 'Boutique updated successfully');
    }

    /**
     * Remove the specified Boutique from storage.
     * DELETE /boutiques/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Boutique $boutique */
        $boutique = $this->boutiqueRepository->find($id);

        if (empty($boutique)) {
            return $this->sendError('Boutique not found');
        }

        $boutique->delete();

        return $this->sendSuccess('Boutique deleted successfully');
    }
}
