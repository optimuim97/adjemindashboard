<?php

namespace App\DataTables;

use App\Models\Boutique;

/**
 * Class BoutiqueDataTable
 */
class BoutiqueDataTable
{
    /**
     * @return Boutique
     */
    public function get()
    {
        /** @var Boutique $query */
        $query = Boutique::query()->select('boutiques.*');

        return $query;
    }
}
