<?php

namespace App\DataTables;

use App\Models\Order;

/**
 * Class OrderDataTable
 */
class OrderDataTable
{
    /**
     * @return Order
     */
    public function get()
    {
        /** @var Order $query */
        $query = Order::query()->select('orders.*');

        return $query;
    }
}
