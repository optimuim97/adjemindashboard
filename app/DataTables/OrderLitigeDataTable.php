<?php

namespace App\DataTables;

use App\Models\OrderLitige;

/**
 * Class OrderLitigeDataTable
 */
class OrderLitigeDataTable
{
    /**
     * @return OrderLitige
     */
    public function get()
    {
        /** @var OrderLitige $query */
        $query = OrderLitige::query()->select('order_litiges.*');

        return $query;
    }
}
