<?php

namespace App\DataTables;

use App\Models\Camion;

/**
 * Class CamionDataTable
 */
class CamionDataTable
{
    /**
     * @return Camion
     */
    public function get()
    {
        /** @var Camion $query */
        $query = Camion::query()->select('camions.*');

        return $query;
    }
}
