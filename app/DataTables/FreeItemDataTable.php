<?php

namespace App\DataTables;

use App\Models\FreeItem;

/**
 * Class FreeItemDataTable
 */
class FreeItemDataTable
{
    /**
     * @return FreeItem
     */
    public function get()
    {
        /** @var FreeItem $query */
        $query = FreeItem::query()->select('free_items.*');

        return $query;
    }
}
