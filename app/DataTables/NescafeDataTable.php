<?php

namespace App\DataTables;

use App\Models\Nescafe;

/**
 * Class NescafeDataTable
 */
class NescafeDataTable
{
    /**
     * @return Nescafe
     */
    public function get()
    {
        /** @var Nescafe $query */
        $query = Nescafe::query()->select('nescaves.*');

        return $query;
    }
}
