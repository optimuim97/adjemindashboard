<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Order
 * @package App\Models
 * @version August 16, 2021, 6:09 pm UTC
 *
 * @property integer $customer_id
 * @property integer $seller_id
 * @property string $seller_type
 * @property boolean $is_waiting
 * @property string $current_status
 * @property string $rating
 * @property string $notes
 * @property string $note_description
 * @property string|\Carbon\Carbon $note_date
 * @property string $reports
 * @property string $report_description
 * @property string|\Carbon\Carbon $report_date
 * @property string $payment_method_slug
 * @property string $delivery_formula_slug
 * @property string $delivery_fees
 * @property string $amount
 * @property string $currency_code
 * @property boolean $is_delivered
 * @property boolean $is_waiting_payment
 * @property boolean $is_waiting_note
 * @property string|\Carbon\Carbon $delivery_date
 * @property string $task_id
 * @property string $merchant_assignments
 * @property string $merchant_assignment_accepted
 * @property string $reference
 * @property boolean $seller_has_delivred
 * @property boolean $seller_has_confirmed
 * @property boolean $customer_has_confirmed_delivery
 * @property string $customer_delivery_signature
 * @property string|\Carbon\Carbon $customer_delivery_signature_at
 * @property boolean $is_customer_reported
 * @property boolean $is_completed
 * @property boolean $is_seller_paid
 * @property boolean $has_litige
 * @property boolean $is_refunded
 * @property boolean $is_litige_solved
 * @property string $litige_solution
 * @property boolean $is_returned
 * @property boolean $is_customer_satisfied
 * @property integer $warranty_period_days
 * @property string|\Carbon\Carbon $warranty_period_last_date
 * @property string $return_delivery_signature
 * @property string|\Carbon\Carbon $return_delivery_signature_at
 */
class Order extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'orders';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'customer_id',
        'seller_id',
        'seller_type',
        'is_waiting',
        'current_status',
        'rating',
        'notes',
        'note_description',
        'note_date',
        'reports',
        'report_description',
        'report_date',
        'payment_method_slug',
        'delivery_formula_slug',
        'delivery_fees',
        'amount',
        'currency_code',
        'is_delivered',
        'is_waiting_payment',
        'is_waiting_note',
        'delivery_date',
        'task_id',
        'merchant_assignments',
        'merchant_assignment_accepted',
        'reference',
        'seller_has_delivred',
        'seller_has_confirmed',
        'customer_has_confirmed_delivery',
        'customer_delivery_signature',
        'customer_delivery_signature_at',
        'is_customer_reported',
        'is_completed',
        'is_seller_paid',
        'has_litige',
        'is_refunded',
        'is_litige_solved',
        'litige_solution',
        'is_returned',
        'is_customer_satisfied',
        'warranty_period_days',
        'warranty_period_last_date',
        'return_delivery_signature',
        'return_delivery_signature_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'customer_id' => 'integer',
        'seller_id' => 'integer',
        'seller_type' => 'string',
        'is_waiting' => 'boolean',
        'current_status' => 'string',
        'rating' => 'string',
        'notes' => 'string',
        'note_description' => 'string',
        'note_date' => 'datetime',
        'reports' => 'string',
        'report_description' => 'string',
        'report_date' => 'datetime',
        'payment_method_slug' => 'string',
        'delivery_formula_slug' => 'string',
        'delivery_fees' => 'string',
        'amount' => 'string',
        'currency_code' => 'string',
        'is_delivered' => 'boolean',
        'is_waiting_payment' => 'boolean',
        'is_waiting_note' => 'boolean',
        'delivery_date' => 'datetime',
        'task_id' => 'string',
        'merchant_assignments' => 'string',
        'merchant_assignment_accepted' => 'string',
        'reference' => 'string',
        'seller_has_delivred' => 'boolean',
        'seller_has_confirmed' => 'boolean',
        'customer_has_confirmed_delivery' => 'boolean',
        'customer_delivery_signature' => 'string',
        'customer_delivery_signature_at' => 'datetime',
        'is_customer_reported' => 'boolean',
        'is_completed' => 'boolean',
        'is_seller_paid' => 'boolean',
        'has_litige' => 'boolean',
        'is_refunded' => 'boolean',
        'is_litige_solved' => 'boolean',
        'litige_solution' => 'string',
        'is_returned' => 'boolean',
        'is_customer_satisfied' => 'boolean',
        'warranty_period_days' => 'integer',
        'warranty_period_last_date' => 'datetime',
        'return_delivery_signature' => 'string',
        'return_delivery_signature_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'customer_id' => 'nullable',
        'seller_id' => 'nullable',
        'seller_type' => 'nullable|string|max:225',
        'is_waiting' => 'required|boolean',
        'current_status' => 'nullable|string|max:255',
        'rating' => 'nullable|string|max:255',
        'notes' => 'nullable|string',
        'note_description' => 'nullable|string',
        'note_date' => 'nullable',
        'reports' => 'nullable|string',
        'report_description' => 'nullable|string',
        'report_date' => 'nullable',
        'payment_method_slug' => 'required|string|max:255',
        'delivery_formula_slug' => 'nullable|string|max:225',
        'delivery_fees' => 'nullable|string|max:255',
        'amount' => 'nullable|string|max:225',
        'currency_code' => 'nullable|string|max:225',
        'is_delivered' => 'required|boolean',
        'is_waiting_payment' => 'nullable|boolean',
        'is_waiting_note' => 'nullable|boolean',
        'delivery_date' => 'nullable',
        'task_id' => 'nullable|string|max:225',
        'merchant_assignments' => 'nullable|string',
        'merchant_assignment_accepted' => 'nullable|string',
        'reference' => 'nullable|string|max:225',
        'seller_has_delivred' => 'nullable|boolean',
        'seller_has_confirmed' => 'nullable|boolean',
        'customer_has_confirmed_delivery' => 'nullable|boolean',
        'customer_delivery_signature' => 'nullable|string|max:225',
        'customer_delivery_signature_at' => 'nullable',
        'is_customer_reported' => 'nullable|boolean',
        'is_completed' => 'nullable|boolean',
        'is_seller_paid' => 'nullable|boolean',
        'has_litige' => 'nullable|boolean',
        'is_refunded' => 'nullable|boolean',
        'is_litige_solved' => 'nullable|boolean',
        'litige_solution' => 'nullable|string|max:225',
        'is_returned' => 'nullable|boolean',
        'is_customer_satisfied' => 'nullable|boolean',
        'warranty_period_days' => 'nullable|integer',
        'warranty_period_last_date' => 'nullable',
        'return_delivery_signature' => 'nullable|string|max:225',
        'return_delivery_signature_at' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    
}
