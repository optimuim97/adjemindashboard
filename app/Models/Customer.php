<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Customer
 * @package App\Models
 * @version August 16, 2021, 5:38 pm UTC
 *
 * @property string $first_name
 * @property string $last_name
 * @property string $name
 * @property string $phone_number
 * @property string $phone
 * @property boolean $is_phone_verified
 * @property string|\Carbon\Carbon $phone_verified_at
 * @property boolean $is_active
 * @property string|\Carbon\Carbon $activation_date
 * @property string $dial_code
 * @property string $country_code
 * @property string $email
 * @property string|\Carbon\Carbon $email_verified_at
 * @property integer $photo_id
 * @property string $photo_url
 * @property string $photo_mime
 * @property string $photo_height
 * @property string $photo_width
 * @property string $photo_length
 * @property string $gender
 * @property string $bio
 * @property string $birthday
 * @property string $birth_location
 * @property string $language
 * @property string $facebook_id
 * @property string $twitter_id
 * @property string $google_id
 * @property boolean $is_pro_seller
 * @property boolean $has_adjemin_pay
 * @property string $origin
 * @property string $password
 * @property string $wallet_password
 * @property boolean $is_wallet_active
 * @property string|\Carbon\Carbon $wallet_activation_date
 * @property string $default_currency
 * @property string $identity_card_ref
 * @property string $identity_card_link_recto
 * @property string $identity_card_link_verso
 * @property string $identity_card_type
 * @property string $account_type
 * @property string $company_name
 * @property string $company_country
 * @property string $company_reference
 * @property string $last_location_gps
 * @property integer $rating
 * @property string $username
 * @property string $godfather_username
 * @property boolean $has_badge
 * @property boolean $is_email_registration
 */
class Customer extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'customers';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'first_name',
        'last_name',
        'name',
        'phone_number',
        'phone',
        'is_phone_verified',
        'phone_verified_at',
        'is_active',
        'activation_date',
        'dial_code',
        'country_code',
        'email',
        'email_verified_at',
        'photo_id',
        'photo_url',
        'photo_mime',
        'photo_height',
        'photo_width',
        'photo_length',
        'gender',
        'bio',
        'birthday',
        'birth_location',
        'language',
        'facebook_id',
        'twitter_id',
        'google_id',
        'is_pro_seller',
        'has_adjemin_pay',
        'origin',
        'password',
        'wallet_password',
        'is_wallet_active',
        'wallet_activation_date',
        'default_currency',
        'identity_card_ref',
        'identity_card_link_recto',
        'identity_card_link_verso',
        'identity_card_type',
        'account_type',
        'company_name',
        'company_country',
        'company_reference',
        'last_location_gps',
        'rating',
        'username',
        'godfather_username',
        'has_badge',
        'is_email_registration'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'first_name' => 'string',
        'last_name' => 'string',
        'name' => 'string',
        'phone_number' => 'string',
        'phone' => 'string',
        'is_phone_verified' => 'boolean',
        'phone_verified_at' => 'datetime',
        'is_active' => 'boolean',
        'activation_date' => 'datetime',
        'dial_code' => 'string',
        'country_code' => 'string',
        'email' => 'string',
        'email_verified_at' => 'datetime',
        'photo_id' => 'integer',
        'photo_url' => 'string',
        'photo_mime' => 'string',
        'photo_height' => 'string',
        'photo_width' => 'string',
        'photo_length' => 'string',
        'gender' => 'string',
        'bio' => 'string',
        'birthday' => 'string',
        'birth_location' => 'string',
        'language' => 'string',
        'facebook_id' => 'string',
        'twitter_id' => 'string',
        'google_id' => 'string',
        'is_pro_seller' => 'boolean',
        'has_adjemin_pay' => 'boolean',
        'origin' => 'string',
        'password' => 'string',
        'wallet_password' => 'string',
        'is_wallet_active' => 'boolean',
        'wallet_activation_date' => 'datetime',
        'default_currency' => 'string',
        'identity_card_ref' => 'string',
        'identity_card_link_recto' => 'string',
        'identity_card_link_verso' => 'string',
        'identity_card_type' => 'string',
        'account_type' => 'string',
        'company_name' => 'string',
        'company_country' => 'string',
        'company_reference' => 'string',
        'last_location_gps' => 'string',
        'rating' => 'integer',
        'username' => 'string',
        'godfather_username' => 'string',
        'has_badge' => 'boolean',
        'is_email_registration' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'first_name' => 'nullable|string|max:255',
        'last_name' => 'nullable|string|max:255',
        'name' => 'nullable|string|max:255',
        'phone_number' => 'nullable|string|max:255',
        'phone' => 'required|string|max:255',
        'is_phone_verified' => 'nullable|boolean',
        'phone_verified_at' => 'nullable',
        'is_active' => 'nullable|boolean',
        'activation_date' => 'nullable',
        'dial_code' => 'nullable|string|max:255',
        'country_code' => 'nullable|string|max:255',
        'email' => 'nullable|string|max:255',
        'email_verified_at' => 'nullable',
        'photo_id' => 'nullable',
        'photo_url' => 'nullable|string|max:255',
        'photo_mime' => 'nullable|string|max:255',
        'photo_height' => 'nullable|string|max:255',
        'photo_width' => 'nullable|string|max:255',
        'photo_length' => 'nullable|string|max:255',
        'gender' => 'nullable|string|max:255',
        'bio' => 'nullable|string|max:255',
        'birthday' => 'nullable|string|max:255',
        'birth_location' => 'nullable|string|max:225',
        'language' => 'nullable|string|max:255',
        'facebook_id' => 'nullable|string|max:255',
        'twitter_id' => 'nullable|string|max:255',
        'google_id' => 'nullable|string|max:225',
        'is_pro_seller' => 'required|boolean',
        'has_adjemin_pay' => 'nullable|boolean',
        'origin' => 'nullable|string|max:225',
        'password' => 'nullable|string|max:225',
        'wallet_password' => 'nullable|string|max:225',
        'is_wallet_active' => 'nullable|boolean',
        'wallet_activation_date' => 'nullable',
        'default_currency' => 'nullable|string|max:225',
        'identity_card_ref' => 'nullable|string|max:225',
        'identity_card_link_recto' => 'nullable|string|max:225',
        'identity_card_link_verso' => 'nullable|string|max:225',
        'identity_card_type' => 'nullable|string|max:225',
        'account_type' => 'nullable|string|max:225',
        'company_name' => 'nullable|string|max:225',
        'company_country' => 'nullable|string|max:225',
        'company_reference' => 'nullable|string|max:225',
        'last_location_gps' => 'nullable|string|max:225',
        'rating' => 'nullable|integer',
        'username' => 'nullable|string|max:191',
        'godfather_username' => 'nullable|string|max:225',
        'has_badge' => 'nullable|boolean',
        'is_email_registration' => 'nullable|boolean',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    
}
