<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Boutique
 * @package App\Models
 * @version August 16, 2021, 6:32 pm UTC
 *
 * @property string $nom
 * @property string $email
 * @property string $identifiant
 * @property string $rccm
 * @property integer $customer_id
 * @property string $photo_profil
 */
class Boutique extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'boutiques';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'nom',
        'email',
        'identifiant',
        'rccm',
        'customer_id',
        'photo_profil'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nom' => 'string',
        'email' => 'string',
        'identifiant' => 'string',
        'rccm' => 'string',
        'customer_id' => 'integer',
        'photo_profil' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nom' => 'nullable|string|max:191',
        'email' => 'nullable|string|max:100',
        'identifiant' => 'nullable|string|max:100',
        'rccm' => 'nullable|string|max:30',
        'customer_id' => 'nullable|integer',
        'photo_profil' => 'nullable|string|max:191',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    
}
