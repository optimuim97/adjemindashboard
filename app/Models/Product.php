<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Product
 * @package App\Models
 * @version August 16, 2021, 5:43 pm UTC
 *
 * @property string $title
 * @property string $description
 * @property string $description_metadata
 * @property string $price
 * @property string $original_price
 * @property string $fees
 * @property string $currency_slug
 * @property string $old_price
 * @property boolean $has_promo
 * @property string $promo_percentage
 * @property string $medias
 * @property integer $cover_id
 * @property string $cover_type
 * @property string $cover_url
 * @property string $cover_thumbnail
 * @property string $cover_duration
 * @property string $cover_width
 * @property string $cover_height
 * @property string $cover_blurhash
 * @property string $location_name
 * @property string $location_address
 * @property number $location_lat
 * @property number $location_lng
 * @property integer $category_id
 * @property string $category_meta
 * @property integer $customer_id
 * @property boolean $is_from_shop
 * @property boolean $is_sold
 * @property string|\Carbon\Carbon $sold_at
 * @property string $initiale_count
 * @property boolean $is_firm_price
 * @property integer $condition_id
 * @property string|\Carbon\Carbon $published_at
 * @property boolean $is_published
 * @property integer $deleted_by
 * @property string $deleted_creator
 * @property string $sold_count
 * @property string $delivery_services
 * @property string $slug
 * @property string $link
 * @property boolean $has_delivery
 * @property boolean $has_ordering
 * @property string $order_payment_methods
 * @property string|\Carbon\Carbon $visibility_deadline
 * @property string $tags
 * @property boolean $is_garentie
 * @property integer $duree_garentie
 * @property boolean $is_gain_shop
 * @property boolean $is_exclusive_price
 * @property boolean $is_promo_gain_shop
 * @property boolean $is_selling
 * @property string $promo_gain_shop
 * @property string $promo_gain_shop_price
 * @property boolean $is_night_market
 * @property string $promo_night_market_price
 * @property string $promo_night_market_percentage
 */
class Product extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'products';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'description',
        'description_metadata',
        'price',
        'original_price',
        'fees',
        'currency_slug',
        'old_price',
        'has_promo',
        'promo_percentage',
        'medias',
        'cover_id',
        'cover_type',
        'cover_url',
        'cover_thumbnail',
        'cover_duration',
        'cover_width',
        'cover_height',
        'cover_blurhash',
        'location_name',
        'location_address',
        'location_lat',
        'location_lng',
        'category_id',
        'category_meta',
        'customer_id',
        'is_from_shop',
        'is_sold',
        'sold_at',
        'initiale_count',
        'is_firm_price',
        'condition_id',
        'published_at',
        'is_published',
        'deleted_by',
        'deleted_creator',
        'sold_count',
        'delivery_services',
        'slug',
        'link',
        'has_delivery',
        'has_ordering',
        'order_payment_methods',
        'visibility_deadline',
        'tags',
        'is_garentie',
        'duree_garentie',
        'is_gain_shop',
        'is_exclusive_price',
        'is_promo_gain_shop',
        'is_selling',
        'promo_gain_shop',
        'promo_gain_shop_price',
        'is_night_market',
        'promo_night_market_price',
        'promo_night_market_percentage'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'description' => 'string',
        'description_metadata' => 'string',
        'price' => 'string',
        'original_price' => 'string',
        'fees' => 'string',
        'currency_slug' => 'string',
        'old_price' => 'string',
        'has_promo' => 'boolean',
        'promo_percentage' => 'string',
        'medias' => 'string',
        'cover_id' => 'integer',
        'cover_type' => 'string',
        'cover_url' => 'string',
        'cover_thumbnail' => 'string',
        'cover_duration' => 'string',
        'cover_width' => 'string',
        'cover_height' => 'string',
        'cover_blurhash' => 'string',
        'location_name' => 'string',
        'location_address' => 'string',
        'location_lat' => 'float',
        'location_lng' => 'float',
        'category_id' => 'integer',
        'category_meta' => 'string',
        'customer_id' => 'integer',
        'is_from_shop' => 'boolean',
        'is_sold' => 'boolean',
        'sold_at' => 'datetime',
        'initiale_count' => 'string',
        'is_firm_price' => 'boolean',
        'condition_id' => 'integer',
        'published_at' => 'datetime',
        'is_published' => 'boolean',
        'deleted_by' => 'integer',
        'deleted_creator' => 'string',
        'sold_count' => 'string',
        'delivery_services' => 'string',
        'slug' => 'string',
        'link' => 'string',
        'has_delivery' => 'boolean',
        'has_ordering' => 'boolean',
        'order_payment_methods' => 'string',
        'visibility_deadline' => 'datetime',
        'tags' => 'string',
        'is_garentie' => 'boolean',
        'duree_garentie' => 'integer',
        'is_gain_shop' => 'boolean',
        'is_exclusive_price' => 'boolean',
        'is_promo_gain_shop' => 'boolean',
        'is_selling' => 'boolean',
        'promo_gain_shop' => 'string',
        'promo_gain_shop_price' => 'string',
        'is_night_market' => 'boolean',
        'promo_night_market_price' => 'string',
        'promo_night_market_percentage' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'nullable|string|max:255',
        'description' => 'nullable|string',
        'description_metadata' => 'nullable|string',
        'price' => 'nullable|string|max:225',
        'original_price' => 'nullable|string|max:225',
        'fees' => 'nullable|string|max:225',
        'currency_slug' => 'nullable|string|max:225',
        'old_price' => 'nullable|string|max:225',
        'has_promo' => 'nullable|boolean',
        'promo_percentage' => 'nullable|string|max:225',
        'medias' => 'nullable|string',
        'cover_id' => 'nullable',
        'cover_type' => 'nullable|string|max:255',
        'cover_url' => 'nullable|string|max:255',
        'cover_thumbnail' => 'nullable|string|max:255',
        'cover_duration' => 'nullable|string|max:255',
        'cover_width' => 'nullable|string|max:255',
        'cover_height' => 'nullable|string|max:255',
        'cover_blurhash' => 'nullable|string|max:225',
        'location_name' => 'nullable|string|max:255',
        'location_address' => 'nullable|string|max:255',
        'location_lat' => 'nullable|numeric',
        'location_lng' => 'nullable|numeric',
        'category_id' => 'nullable',
        'category_meta' => 'nullable|string',
        'customer_id' => 'nullable',
        'is_from_shop' => 'nullable|boolean',
        'is_sold' => 'required|boolean',
        'sold_at' => 'nullable',
        'initiale_count' => 'required|string|max:255',
        'is_firm_price' => 'required|boolean',
        'condition_id' => 'nullable|integer',
        'published_at' => 'nullable',
        'is_published' => 'required|boolean',
        'deleted_by' => 'nullable',
        'deleted_creator' => 'nullable|string|max:255',
        'sold_count' => 'required|string|max:255',
        'delivery_services' => 'nullable|string',
        'slug' => 'nullable|string|max:225',
        'link' => 'nullable|string|max:225',
        'has_delivery' => 'nullable|boolean',
        'has_ordering' => 'nullable|boolean',
        'order_payment_methods' => 'nullable|string',
        'visibility_deadline' => 'nullable',
        'tags' => 'nullable|string',
        'is_garentie' => 'nullable|boolean',
        'duree_garentie' => 'nullable|integer',
        'is_gain_shop' => 'nullable|boolean',
        'is_exclusive_price' => 'nullable|boolean',
        'is_promo_gain_shop' => 'nullable|boolean',
        'is_selling' => 'nullable|boolean',
        'promo_gain_shop' => 'nullable|string|max:225',
        'promo_gain_shop_price' => 'nullable|string|max:225',
        'is_night_market' => 'nullable|boolean',
        'promo_night_market_price' => 'nullable|string|max:225',
        'promo_night_market_percentage' => 'nullable|string|max:225',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    
}
