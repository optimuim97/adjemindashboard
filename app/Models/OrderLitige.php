<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;


/**
 * Class OrderLitige
 * @package App\Models
 * @version August 18, 2021, 4:47 pm UTC
 *
 * @property integer $order_id
 * @property integer $customer_id
 * @property integer $seller_id
 * @property string $customer_solution
 * @property string $seller_solution
 * @property string $seller_note
 * @property string $final_litige_solution
 * @property boolean $litige_accepted
 * @property integer $feedback_id
 * @property string $litige_rejected_reason
 * @property boolean $solved
 * @property boolean $is_completed
 * @property boolean $is_waiting_admin_acceptation
 */
class OrderLitige extends Model
{
    use SoftDeletes;

    use HasFactory;
    protected $appends = ['customer', 'seller'];


    public $table = 'order_litiges';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    // public $fillable = [
    //     'order_id',
    //     'customer_id',
    //     'seller_id',
    //     'customer_solution',
    //     'seller_solution',
    //     'seller_note',
    //     'final_litige_solution',
    //     'litige_accepted',
    //     'feedback_id',
    //     'litige_rejected_reason',
    //     'solved',
    //     'is_completed',
    //     'is_waiting_admin_acceptation'
    // ];

    public $guarded = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'order_id' => 'integer',
        'customer_id' => 'integer',
        'seller_id' => 'integer',
        'customer_solution' => 'string',
        'seller_solution' => 'string',
        'seller_note' => 'string',
        'final_litige_solution' => 'string',
        'litige_accepted' => 'boolean',
        'feedback_id' => 'integer',
        'litige_rejected_reason' => 'string',
        'solved' => 'boolean',
        'is_completed' => 'boolean',
        'is_waiting_admin_acceptation' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'order_id' => 'nullable',
        'customer_id' => 'nullable',
        'seller_id' => 'nullable',
        'customer_solution' => 'nullable|string|max:225',
        'seller_solution' => 'nullable|string|max:225',
        'seller_note' => 'nullable|string',
        'final_litige_solution' => 'nullable|string|max:225',
        'litige_accepted' => 'nullable|boolean',
        'feedback_id' => 'nullable',
        'litige_rejected_reason' => 'nullable|string',
        'solved' => 'nullable|boolean',
        'is_completed' => 'nullable|boolean',
        'is_waiting_admin_acceptation' => 'nullable|boolean',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function getCustomerAttribute(){
        $customer = Customer::where('id', $this->customer_id)->first();
        return $customer;
    }

    public function getSellerAttribute(){
        $customer = Customer::where('id', $this->seller_id)->first();
        return $customer;
    }


}
