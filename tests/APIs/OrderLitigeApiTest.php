<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\OrderLitige;

class OrderLitigeApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_order_litige()
    {
        $orderLitige = OrderLitige::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/order_litiges', $orderLitige
        );

        $this->assertApiResponse($orderLitige);
    }

    /**
     * @test
     */
    public function test_read_order_litige()
    {
        $orderLitige = OrderLitige::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/order_litiges/'.$orderLitige->id
        );

        $this->assertApiResponse($orderLitige->toArray());
    }

    /**
     * @test
     */
    public function test_update_order_litige()
    {
        $orderLitige = OrderLitige::factory()->create();
        $editedOrderLitige = OrderLitige::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/order_litiges/'.$orderLitige->id,
            $editedOrderLitige
        );

        $this->assertApiResponse($editedOrderLitige);
    }

    /**
     * @test
     */
    public function test_delete_order_litige()
    {
        $orderLitige = OrderLitige::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/order_litiges/'.$orderLitige->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/order_litiges/'.$orderLitige->id
        );

        $this->response->assertStatus(404);
    }
}
