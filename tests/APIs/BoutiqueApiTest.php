<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Boutique;

class BoutiqueApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_boutique()
    {
        $boutique = Boutique::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/boutiques', $boutique
        );

        $this->assertApiResponse($boutique);
    }

    /**
     * @test
     */
    public function test_read_boutique()
    {
        $boutique = Boutique::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/boutiques/'.$boutique->id
        );

        $this->assertApiResponse($boutique->toArray());
    }

    /**
     * @test
     */
    public function test_update_boutique()
    {
        $boutique = Boutique::factory()->create();
        $editedBoutique = Boutique::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/boutiques/'.$boutique->id,
            $editedBoutique
        );

        $this->assertApiResponse($editedBoutique);
    }

    /**
     * @test
     */
    public function test_delete_boutique()
    {
        $boutique = Boutique::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/boutiques/'.$boutique->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/boutiques/'.$boutique->id
        );

        $this->response->assertStatus(404);
    }
}
