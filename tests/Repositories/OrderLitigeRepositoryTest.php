<?php namespace Tests\Repositories;

use App\Models\OrderLitige;
use App\Repositories\OrderLitigeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class OrderLitigeRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var OrderLitigeRepository
     */
    protected $orderLitigeRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->orderLitigeRepo = \App::make(OrderLitigeRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_order_litige()
    {
        $orderLitige = OrderLitige::factory()->make()->toArray();

        $createdOrderLitige = $this->orderLitigeRepo->create($orderLitige);

        $createdOrderLitige = $createdOrderLitige->toArray();
        $this->assertArrayHasKey('id', $createdOrderLitige);
        $this->assertNotNull($createdOrderLitige['id'], 'Created OrderLitige must have id specified');
        $this->assertNotNull(OrderLitige::find($createdOrderLitige['id']), 'OrderLitige with given id must be in DB');
        $this->assertModelData($orderLitige, $createdOrderLitige);
    }

    /**
     * @test read
     */
    public function test_read_order_litige()
    {
        $orderLitige = OrderLitige::factory()->create();

        $dbOrderLitige = $this->orderLitigeRepo->find($orderLitige->id);

        $dbOrderLitige = $dbOrderLitige->toArray();
        $this->assertModelData($orderLitige->toArray(), $dbOrderLitige);
    }

    /**
     * @test update
     */
    public function test_update_order_litige()
    {
        $orderLitige = OrderLitige::factory()->create();
        $fakeOrderLitige = OrderLitige::factory()->make()->toArray();

        $updatedOrderLitige = $this->orderLitigeRepo->update($fakeOrderLitige, $orderLitige->id);

        $this->assertModelData($fakeOrderLitige, $updatedOrderLitige->toArray());
        $dbOrderLitige = $this->orderLitigeRepo->find($orderLitige->id);
        $this->assertModelData($fakeOrderLitige, $dbOrderLitige->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_order_litige()
    {
        $orderLitige = OrderLitige::factory()->create();

        $resp = $this->orderLitigeRepo->delete($orderLitige->id);

        $this->assertTrue($resp);
        $this->assertNull(OrderLitige::find($orderLitige->id), 'OrderLitige should not exist in DB');
    }
}
