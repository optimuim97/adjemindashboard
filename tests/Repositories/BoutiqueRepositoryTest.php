<?php namespace Tests\Repositories;

use App\Models\Boutique;
use App\Repositories\BoutiqueRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class BoutiqueRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var BoutiqueRepository
     */
    protected $boutiqueRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->boutiqueRepo = \App::make(BoutiqueRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_boutique()
    {
        $boutique = Boutique::factory()->make()->toArray();

        $createdBoutique = $this->boutiqueRepo->create($boutique);

        $createdBoutique = $createdBoutique->toArray();
        $this->assertArrayHasKey('id', $createdBoutique);
        $this->assertNotNull($createdBoutique['id'], 'Created Boutique must have id specified');
        $this->assertNotNull(Boutique::find($createdBoutique['id']), 'Boutique with given id must be in DB');
        $this->assertModelData($boutique, $createdBoutique);
    }

    /**
     * @test read
     */
    public function test_read_boutique()
    {
        $boutique = Boutique::factory()->create();

        $dbBoutique = $this->boutiqueRepo->find($boutique->id);

        $dbBoutique = $dbBoutique->toArray();
        $this->assertModelData($boutique->toArray(), $dbBoutique);
    }

    /**
     * @test update
     */
    public function test_update_boutique()
    {
        $boutique = Boutique::factory()->create();
        $fakeBoutique = Boutique::factory()->make()->toArray();

        $updatedBoutique = $this->boutiqueRepo->update($fakeBoutique, $boutique->id);

        $this->assertModelData($fakeBoutique, $updatedBoutique->toArray());
        $dbBoutique = $this->boutiqueRepo->find($boutique->id);
        $this->assertModelData($fakeBoutique, $dbBoutique->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_boutique()
    {
        $boutique = Boutique::factory()->create();

        $resp = $this->boutiqueRepo->delete($boutique->id);

        $this->assertTrue($resp);
        $this->assertNull(Boutique::find($boutique->id), 'Boutique should not exist in DB');
    }
}
